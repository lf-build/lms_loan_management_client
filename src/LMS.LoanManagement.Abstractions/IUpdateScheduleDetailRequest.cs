﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public interface IUpdateScheduleDetailRequest
    {
        int InstallmentNumber { get; set; }
        double PaidAmount { get; set; }
        DateTimeOffset PaidDate { get; set; }
    }
}