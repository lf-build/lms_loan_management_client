namespace LMS.LoanManagement.Abstractions
{
    public class BorrowerEmailData : IBorrowerEmailData
    {
        public string EntityType {get;set;}
        public string BorrowerId {get;set;}
        public string PortalUrl {get;set;}
        public string BorrowerEmail {get;set;}
        public string TemplateName {get;set;}
        public string TemplateVersion {get;set;}
        public string BorrowerName {get;set;}
    }
}