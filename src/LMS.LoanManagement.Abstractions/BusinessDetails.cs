﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json;

namespace LMS.LoanManagement.Abstractions {
    public class BusinessDetails : IBusinessDetails {
        public BusinessDetails () {

        }
        public BusinessDetails (IBusinessRequest businessRequest) {
            if (businessRequest == null)
                return;
            Id = Guid.NewGuid ().ToString ("N");
            Address = businessRequest.Address != null ? new Address (businessRequest.Address) : null;
            BusinessName = businessRequest.BusinessName;
            DBA = businessRequest.DBA;
            Email = businessRequest.Email != null ? new Email (businessRequest.Email) : null;
            FedTaxId = businessRequest.FedTaxId;
            Phone = businessRequest.Phone != null ? new Phone (businessRequest.Phone) : null;
            EstablishedDate = businessRequest.EstablishedDate != null ? new TimeBucket (businessRequest.EstablishedDate) : new TimeBucket (DateTime.MinValue);
            BusinessIdentifiers =businessRequest.BusinessIdentifiers!= null? businessRequest.BusinessIdentifiers.Select (i => new Identifier (i)).ToList<IIdentifier> (): null;
            BusinessMaxLimitAmount=businessRequest.BusinessMaxLimitAmount;


        }
        public BusinessDetails (IBusinessDetails businessDetails) {
            if (businessDetails == null)
                return;
            Id = businessDetails.Id;
            Address = businessDetails.Address != null ? new Address (businessDetails.Address) : null;
            BusinessName = businessDetails.BusinessName;
            DBA = businessDetails.DBA;
            Email = businessDetails.Email != null ? new Email (businessDetails.Email) : null;
            FedTaxId = businessDetails.FedTaxId;
            Phone = businessDetails.Phone != null ? new Phone (businessDetails.Phone) : null;
            EstablishedDate = businessDetails.EstablishedDate != null ? businessDetails.EstablishedDate : new TimeBucket (DateTime.MinValue);
            BusinessIdentifiers =businessDetails.BusinessIdentifiers!= null? businessDetails.BusinessIdentifiers.Select (i => new Identifier (i)).ToList<IIdentifier> ():null;
            BusinessMaxLimitAmount=businessDetails.BusinessMaxLimitAmount;


        }
        public string Id { get; set; }
        public string BusinessName { get; set; }
        public string DBA { get; set; }
        public string FedTaxId { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IAddress, Address>))]
        public IAddress Address { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IPhone, Phone>))]
        public IPhone Phone { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IEmail, Email>))]
        public IEmail Email { get; set; }
        public TimeBucket EstablishedDate { get; set; }
        public double BusinessMaxLimitAmount{get;set;}

        [JsonConverter (typeof (InterfaceListConverter<IIdentifier, Identifier>))]
        public List<IIdentifier> BusinessIdentifiers { get; set; }
    }
}