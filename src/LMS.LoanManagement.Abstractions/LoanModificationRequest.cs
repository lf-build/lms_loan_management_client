﻿using LendFoundry.Foundation.Client;
using LMS.Foundation.Amortization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public class LoanModificationRequest : ILoanModificationRequest
    {
        public double LoanAmount { get; set; }
        public double PaymentAmount { get; set; }
        public int Term { get; set; }
        public double AnnualRate { get; set; }
        public double FactorRate { get; set; }
        public PaymentFrequency PaymentFrequency { get; set; }
        public DateTimeOffset ScheduleStartDate { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPaybackTier, PaybackTier>))]
        public List<IPaybackTier> PaybackTiers { get; set; }
        //[JsonConverter(typeof(InterfaceListConverter<IPaymentStream, PaymentStream>))]
        public List<PaymentStream> PaymentStreams { get; set; }
        public InterestAdjustmentType InterestAdjustmentType { get; set; }
        public double FinanceCharge { get; set; }
        public double InterestToRecover { get; set; }
        public LoanModType LoanModType { get; set; }

    }
}
