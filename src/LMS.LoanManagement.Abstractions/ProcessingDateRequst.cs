﻿namespace LMS.LoanManagement.Abstractions
{
    public class ProcessingDateRequst : IProcessingDateRequst
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }

    }
}
