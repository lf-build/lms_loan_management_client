﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Abstractions
{
    public interface IBankDetailsRepository : IRepository<IBankDetails>
    {
        Task<IBankDetails> GetBankDetails(string loanNumber, string bankId);
        Task<List<IBankDetails>> GetAllBankDetails(string loanNumber);
        Task<IBankDetails> UpdateBankDetails(string loanNumber, string bankId, IBankDetails bankDetails);
        Task<List<IBankDetails>> GetAllBankList();
    }
}