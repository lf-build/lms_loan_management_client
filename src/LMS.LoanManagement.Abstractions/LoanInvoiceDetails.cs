using System;

namespace LMS.LoanManagement.Abstractions
{
    public class LoanInvoiceDetails : ILoanInvoiceDetails
    {
        public LoanInvoiceDetails()
        {
        }

        public LoanInvoiceDetails(ILoanInvoiceDetails invoiceDetails)
        {
            if (invoiceDetails == null)
            {
                return;
            }

            Id = Guid.NewGuid().ToString("N");
            InvoiceID = invoiceDetails.InvoiceID;
            IssuerName = invoiceDetails.IssuerName;
            CreditDaysOfInvoice = invoiceDetails.CreditDaysOfInvoice;
            InvoiceAmount = invoiceDetails.InvoiceAmount;
            DateOfInvoice = invoiceDetails.DateOfInvoice;
            DueDateOfInvoice = invoiceDetails.DueDateOfInvoice;
        }

        public LoanInvoiceDetails(string id, ILoanInvoiceDetails invoiceDetails)
        {
            if (string.IsNullOrEmpty(id))
            {
                return;
            }

            Id = id;
            InvoiceID = invoiceDetails.InvoiceID;
            IssuerName = invoiceDetails.IssuerName;
            CreditDaysOfInvoice = invoiceDetails.CreditDaysOfInvoice;
            InvoiceAmount = invoiceDetails.InvoiceAmount;
            DateOfInvoice = invoiceDetails.DateOfInvoice;
            DueDateOfInvoice = invoiceDetails.DueDateOfInvoice;
        }

        public string Id { get; set; }
        public string InvoiceID { get; set; }
        public string IssuerName { get; set; }
        public int? CreditDaysOfInvoice { get; set; }
        public double? InvoiceAmount { get; set; }
        public DateTimeOffset? DateOfInvoice { get; set; }
        public DateTimeOffset? DueDateOfInvoice { get; set; }
    }
}