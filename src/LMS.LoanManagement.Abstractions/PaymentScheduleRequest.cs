﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public class PaymentScheduleRequest : IPaymentScheduleRequest
    {
        public DateTimeOffset ScheduleDate { get; set; }
        public int InstallmentNumber { get; set; }
    }
}
