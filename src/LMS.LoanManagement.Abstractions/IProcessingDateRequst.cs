﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Abstractions
{
   public interface IProcessingDateRequst
    {
         string StartDate { get; set; }
         string EndDate { get; set; }
    }
}
