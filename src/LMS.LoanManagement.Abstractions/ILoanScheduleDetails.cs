﻿using LMS.Foundation.Amortization;
using System;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanScheduleDetails
    {
        double ClosingPrincipalOutStandingBalance { get; set; }
        double CumulativeInterestAmount { get; set; }
        double CumulativePaymentAmount { get; set; }
        double CumulativePrincipalAmount { get; set; }
        int Installmentnumber { get; set; }
        int FrequencyDays { get; set; }
        double InterestAmount { get; set; }
        double OpeningPrincipalOutStandingBalance { get; set; }
        double PaymentAmount { get; set; }
        double PrincipalAmount { get; set; }

        double FeeAmount { get; set; }
        DateTime ScheduleDate { get; set; }
        bool IsPaid { get; set; }
        int RefernceInstallmentnumber { get; set; }
        double PaidAmount { get; set; }
        DateTimeOffset PaidDate { get; set; }
        DateTime OriginalScheduleDate { get; set; }
        bool IsMissed { get; set; }
        bool IsProcessed { get; set; }
        InterestHandlingType InterestHandlingType { get; set; }
        double SurplusAmount { get; set; }
        double MinimumAmountDue { get; set; }

    }
}
