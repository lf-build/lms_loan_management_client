﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LMS.LoanManagement.Abstractions
{
    public class ApplicantDetails : IApplicantDetails
    {
        public ApplicantDetails()
        {

        }

        public ApplicantDetails(IApplicantRequest applicantRequest)
        {
            if (applicantRequest == null)
                return;
            Id = Guid.NewGuid().ToString("N");
            if (applicantRequest.Addresses != null)
            {
                Addresses = new List<IAddress>();
                foreach (var address in applicantRequest.Addresses)
                {
                    Addresses.Add(new Address(address));
                }
            }
            if (applicantRequest.Emails != null)
            {
                Emails = new List<IEmail>();
                foreach (var email in applicantRequest.Emails)
                {
                    Emails.Add(new Email(email));
                }
            }
            if (applicantRequest.Phones != null)
            {
                Phones = new List<IPhone>();
                foreach (var phone in applicantRequest.Phones)
                {
                    Phones.Add(new Phone(phone));
                }
            }
            Name = applicantRequest.Name != null ? new Name(applicantRequest.Name) : null;
            PII = applicantRequest.PII != null ? new PII(applicantRequest.PII) : null;
            PositionOfLoan = applicantRequest.PositionOfLoan;
            Role = applicantRequest.Role;
        }

        public ApplicantDetails(IApplicantDetails applicantDetails)
        {
            if (applicantDetails == null)
                return;
            Id = applicantDetails.Id;
            if (applicantDetails.Addresses != null)
            {
                Addresses = new List<IAddress>();
                Addresses.AddRange(applicantDetails.Addresses);
            }
            if (applicantDetails.Emails != null)
            {
                Emails = new List<IEmail>();
                Emails.AddRange(applicantDetails.Emails);
            }
            if (applicantDetails.Phones != null)
            {
                Phones = new List<IPhone>();
                Phones.AddRange(applicantDetails.Phones);
            }
            Name = applicantDetails.Name != null ? new Name(applicantDetails.Name) : null;
            PII = applicantDetails.PII != null ? new PII(applicantDetails.PII) : null;
            PositionOfLoan = applicantDetails.PositionOfLoan;
            Role = applicantDetails.Role;
        }

        public string Id { get; set; }
        public string PositionOfLoan { get; set; }
        public string Role { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IName, Name>))]
        public IName Name { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPII, PII>))]
        public IPII PII { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPhone, Phone>))]
        public List<IPhone> Phones { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IEmail, Email>))]
        public List<IEmail> Emails { get; set; }
    }
}
