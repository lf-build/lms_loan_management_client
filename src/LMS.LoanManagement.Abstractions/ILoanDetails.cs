﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanDetails: ILoanInformation
    {
        List<IBankDetails> BankDetails { get; set; }

        List<ILoanSchedule> LoanSchedules { get; set; }

        

    }
}
