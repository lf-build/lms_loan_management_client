﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Abstractions
{
    public interface IAutoPayRequest
    {
        bool IsAutopay { get; set; }
        DateTimeOffset? AutoPayStartDate { get; set; }
        DateTimeOffset? AutoPayStopDate { get; set; }
         AutoPayType AutoPayType{ get; set; }
    }
}
