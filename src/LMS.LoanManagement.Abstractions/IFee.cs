﻿namespace LMS.LoanManagement.Abstractions
{
    public interface IFee
    {
        bool Accrue { get; set; }
        bool Applied { get; set; }
        double DailyFrequencyAccrualAmount { get; set; }
        double FeeAmount { get; set; }
        string Id { get; set; }
        string FeeName { get; set; }
        string FeeType { get; set; }
        double PaymentFrequencyAccrualAmount { get; set; }
        bool TaxApplicableFlag{get;set;}
    }
}