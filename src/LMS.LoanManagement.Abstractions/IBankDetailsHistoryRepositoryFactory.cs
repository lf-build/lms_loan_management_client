﻿using LendFoundry.Security.Tokens;

namespace LMS.LoanManagement.Abstractions
{
    public interface IBankDetailsHistoryRepositoryFactory
    {
        IBankDetailsHistoryRepository Create(ITokenReader reader);
    }
}