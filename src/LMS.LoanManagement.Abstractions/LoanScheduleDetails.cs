﻿using LMS.Foundation.Amortization;
using System;

namespace LMS.LoanManagement.Abstractions
{
    public class LoanScheduleDetails : ILoanScheduleDetails
    {
        public LoanScheduleDetails()
        {
        }

        public LoanScheduleDetails(ILoanScheduleDetail loanScheduleDetail)
        {
            if (loanScheduleDetail == null)
                return;
            ClosingPrincipalOutStandingBalance = loanScheduleDetail.ClosingPrincipalOutStandingBalance;
            CumulativeInterestAmount = loanScheduleDetail.CumulativeInterestAmount;
            CumulativePaymentAmount = loanScheduleDetail.CumulativePaymentAmount;
            CumulativePrincipalAmount = loanScheduleDetail.CumulativePrincipalAmount;
            Installmentnumber = loanScheduleDetail.Installmentnumber;
            InterestAmount = loanScheduleDetail.InterestAmount;
            OpeningPrincipalOutStandingBalance = loanScheduleDetail.OpeningPrincipalOutStandingBalance;
            PaymentAmount = loanScheduleDetail.PaymentAmount;
            PrincipalAmount = loanScheduleDetail.PrincipalAmount;
            ScheduleDate = loanScheduleDetail.ScheduleDate.Date;
            OriginalScheduleDate = loanScheduleDetail.OriginalScheduleDate;
            InterestHandlingType = loanScheduleDetail.InterestHandlingType;
            FrequencyDays = loanScheduleDetail.FrequencyDays;
            SurplusAmount = loanScheduleDetail.SurplusAmount;
            MinimumAmountDue=loanScheduleDetail.MinimumAmountDue;
        }

        public LoanScheduleDetails(ILoanScheduleDetails loanScheduleDetail)
        {
            if (loanScheduleDetail == null)
                return;
            ClosingPrincipalOutStandingBalance = loanScheduleDetail.ClosingPrincipalOutStandingBalance;
            CumulativeInterestAmount = loanScheduleDetail.CumulativeInterestAmount;
            CumulativePaymentAmount = loanScheduleDetail.CumulativePaymentAmount;
            CumulativePrincipalAmount = loanScheduleDetail.CumulativePrincipalAmount;
            Installmentnumber = loanScheduleDetail.Installmentnumber;
            InterestAmount = loanScheduleDetail.InterestAmount;
            OpeningPrincipalOutStandingBalance = loanScheduleDetail.OpeningPrincipalOutStandingBalance;
            PaymentAmount = loanScheduleDetail.PaymentAmount;
            PrincipalAmount = loanScheduleDetail.PrincipalAmount;
            ScheduleDate = loanScheduleDetail.ScheduleDate.Date;
            OriginalScheduleDate = loanScheduleDetail.OriginalScheduleDate;
            InterestHandlingType = loanScheduleDetail.InterestHandlingType;
            FrequencyDays = loanScheduleDetail.FrequencyDays;
            SurplusAmount = loanScheduleDetail.SurplusAmount;
            IsPaid = loanScheduleDetail.IsPaid;
            IsProcessed = loanScheduleDetail.IsProcessed;
            IsMissed = loanScheduleDetail.IsMissed;
            RefernceInstallmentnumber = loanScheduleDetail.RefernceInstallmentnumber;
            PaidDate = loanScheduleDetail.PaidDate;
            PaidAmount = loanScheduleDetail.PaidAmount;
            MinimumAmountDue=loanScheduleDetail.MinimumAmountDue;
        }

        public double ClosingPrincipalOutStandingBalance { get; set; }
        public double CumulativeInterestAmount { get; set; }
        public double CumulativePaymentAmount { get; set; }
        public double CumulativePrincipalAmount { get; set; }
        public int Installmentnumber { get; set; }
        public int FrequencyDays { get; set; }
        public double InterestAmount { get; set; }
        public double OpeningPrincipalOutStandingBalance { get; set; }
        public double PaymentAmount { get; set; }
        public double PrincipalAmount { get; set; }
        public double FeeAmount { get; set; }
        public DateTime ScheduleDate { get; set; }
        public bool IsPaid { get; set; }
        public int RefernceInstallmentnumber { get; set; }
        public double PaidAmount { get; set; }
        public DateTimeOffset PaidDate { get; set; }
        public DateTime OriginalScheduleDate { get; set; }
        public bool IsMissed { get; set; }
        public bool IsProcessed { get; set; }
        public InterestHandlingType InterestHandlingType { get; set; }
        public double SurplusAmount { get; set; }
        public double MinimumAmountDue { get; set; }

    }
}
