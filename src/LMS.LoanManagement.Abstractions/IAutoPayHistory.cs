﻿using LendFoundry.Foundation.Date;

namespace LMS.LoanManagement.Abstractions
{
    public interface IAutoPayHistory
    {
        TimeBucket StopDate { get; set; }
        TimeBucket ResumeDate { get; set; }
        bool IsAutoPay { get; set; }
    }
}