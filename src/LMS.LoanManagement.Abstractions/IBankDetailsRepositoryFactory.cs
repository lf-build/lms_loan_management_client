﻿using LendFoundry.Security.Tokens;

namespace LMS.LoanManagement.Abstractions
{
    public interface IBankDetailsRepositoryFactory
    {
        IBankDetailsRepository Create(ITokenReader reader);
    }
}