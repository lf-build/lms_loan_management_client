﻿namespace LMS.LoanManagement.Abstractions
{
    public class EventMapping
    {
        public string Name { get; set; }
        public string LoanNumber { get; set; }
        public string EntityType { get; set; }
        public string Response { get; set; }
        public bool IsStatusChange { get; set; }
    }
}
