﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public class OtherAddress : IOtherAddress
    {
        public OtherAddress()
        {
               
        }
        public OtherAddress(IOtherAddress address)
        {
            if (address == null)
                return;
            Id = Guid.NewGuid().ToString("N");
            AddressType = address.AddressType;
            AddressLine1 = address.AddressLine1;
            AddressLine2 = address.AddressLine2;
            City = address.City;
            State = address.State;
            Zip = address.Zip;
            ContactPerson = address.ContactPerson;
            Relation = address.Relation;
            AvailabilityNotes = address.AvailabilityNotes;
            Active = address.Active;
        }
        public string Id { get; set; }
        public string AddressType { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ContactPerson { get; set; }
        public string Relation { get; set; }
        public string AvailabilityNotes { get; set; }
        public bool Active { get; set; }
    }
}
