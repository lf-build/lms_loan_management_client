﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace LMS.LoanManagement.Abstractions
{
    public class Identifier : IIdentifier
    {
        public Identifier()
        {

        }
        public Identifier(IIdentifier identifier)
        {
            if (identifier == null)
                return;
            Id = identifier.Id;
            Type = identifier.Type;
        }
        public string Id { get; set; }
        public string Type { get; set; }
        
    }
}
