﻿using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Abstractions
{
    public interface IBankDetailsHistoryRepository : IRepository<IBankDetailsHistory>
    {
        Task AddHistory(IBankDetailsHistory bankHistory);
    }
}