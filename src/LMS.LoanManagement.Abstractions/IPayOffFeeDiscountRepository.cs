﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Abstractions
{
    public interface IPayOffFeeDiscountRepository : IRepository<IPayOffFeeDiscount>
    {
        Task<List<IPayOffFeeDiscount>> GetPayOffFeeDiscountSchedule(string loanNumber, string feeType = null);
    }
}