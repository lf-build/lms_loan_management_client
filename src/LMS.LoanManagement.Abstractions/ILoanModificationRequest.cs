﻿using System;
using System.Collections.Generic;
using LMS.Foundation.Amortization;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanModificationRequest
    {
        double FactorRate { get; set; }
        double AnnualRate { get; set; }
        double LoanAmount { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPaybackTier, PaybackTier>))]
        List<IPaybackTier> PaybackTiers { get; set; }
        PaymentFrequency PaymentFrequency { get; set; }
        DateTimeOffset ScheduleStartDate { get; set; }
        int Term { get; set; }
        double PaymentAmount { get; set; }
        //[JsonConverter(typeof(InterfaceListConverter<IPaymentStream, PaymentStream>))]
        List<PaymentStream> PaymentStreams { get; set; }
        InterestAdjustmentType InterestAdjustmentType { get; set; }
        double FinanceCharge { get; set; }
        double InterestToRecover { get; set; }
        LoanModType LoanModType { get; set; }
    }
}