﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public interface IOtherContact
    {
        string Id { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IOtherAddress, OtherAddress>))]
        List<IOtherAddress> Address { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IOtherEmail, OtherEmail>))]
        List<IOtherEmail> Email { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IOtherPhone, OtherPhone>))]
        List<IOtherPhone> Phone { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ICollectionDetail, CollectionDetail>))]
        List<ICollectionDetail> CollectionDetails { get; set; }
    }
}