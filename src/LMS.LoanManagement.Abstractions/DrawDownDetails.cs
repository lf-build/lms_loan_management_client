﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public class DrawDownDetails : IDrawDownDetails
    {
        public string DrawDownNumber { get; set; }
        public DateTime DrawDownDate { get; set; }
        public string LoanScheduleNumber { get; set; }
        public double LoanAmount { get; set; }
        public double TotalDrawDownAmount { get; set; }
    }
}
