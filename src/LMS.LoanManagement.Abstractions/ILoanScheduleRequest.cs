﻿using LendFoundry.Foundation.Client;
using LMS.Foundation.Amortization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanScheduleRequest
    {
        string LoanNumber { get; set; }
        string ScheduleVersionNo { get; set; }
        string ScheduledCreatedReason { get; set; }
        string ScheduleNotes { get; set; }
        double LoanAmount { get; set; }
        double FundedAmount { get; set; }
        DateTimeOffset FundedDate { get; set; }
        DateTimeOffset? FirstPaymentDate { get; set; }
        DateTimeOffset OriginalFirstPaymentDate { get; set; }
        DateTimeOffset? ExpectedFirstPaymentDate { get; set; }
        PaymentFrequency PaymentFrequency { get; set; }
        double PaymentFrequencyRate { get; set; }
        double DailyRate { get; set; }
        double AnnualRate { get; set; }
        int Tenure { get; set; }
        double FactorRate { get; set; }
        double CreditLimit { get; set; }
        string DrawDownNumber { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IFee, Fee>))]
        List<IFee> Fees { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPaybackTier, PaybackTier>))]
        List<IPaybackTier> PaybackTiers { get; set; }
        InterestAdjustmentType InterestAdjustmentType { get; set; }
        InterestHandlingType InterestHandlingType { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPaymentStream, PaymentStream>))]
        List<PaymentStream> PaymentStreams { get; set; }
        List<SlabDetails> SlabDetails { get; set; }
       int Term { get; set; }
       int NumberOfDaysInMonth { get; set; }
       int NumberOfDaysInWeek { get; set; }
       [JsonConverter(typeof(InterfaceListConverter<ILoanScheduleDetails, LoanScheduleDetails>))]
       List<ILoanScheduleDetails> LoanScheduleDetails { get; set; }
       bool IsMigration { get; set; }
       int? BillingDate { get; set; }
       
       /// <summary>
       /// If set to InterestOnlyFollowedByBalloon, interest only schedule will be generated
       /// with a final repayment of principal in the last instalment
       /// </summary>
        ScheduleType ScheduleType { get; set; }
        double MADPercentage { get; set; }
        double MinimumAmountDue { get; set; }


    }
}
