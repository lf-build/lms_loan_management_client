﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public class PayOffFeeScheduleDetails : IPayOffFeeScheduleDetails
    {

        public DateTime PayOffFromDate { get; set; }

        public DateTime PayOffToDate { get; set; }
        public double Amount { get; set; }

    }
}