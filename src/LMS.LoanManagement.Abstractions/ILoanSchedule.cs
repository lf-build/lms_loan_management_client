﻿using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LMS.Foundation.Amortization;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanSchedule : IAggregate
    {
        string LoanNumber { get; set; }
        string LoanScheduleNumber { get; set; }
        string ScheduledCreatedReason { get; set; }
        string ScheduleNotes { get; set; }
        TimeBucket ScheduleVersionDate { get; set; }
        string ScheduleVersionNo { get; set; }
        double LoanAmount { get; set; }
        double FundedAmount { get; set; }
        TimeBucket FundedDate { get; set; }
        TimeBucket FirstPaymentDate { get; set; }
        PaymentFrequency PaymentFrequency { get; set; }
        double PaymentFrequencyRate { get; set; }
        double DailyRate { get; set; }
        double AnnualRate { get; set; }
        int Tenure { get; set; }
        double FactorRate { get; set; }
        bool IsCurrent { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILoanScheduleDetails, LoanScheduleDetails>))]
        List<ILoanScheduleDetails> ScheduleDetails { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILoanScheduleDetail, LoanScheduleDetail>))]
        List<ILoanScheduleDetail> DailyScheduleDetails { get; set; }
        double CreditLimit { get; set; }
        double TotalDrawDown { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IFee, Fee>))]
        List<IFee> Fees { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IFeeDetails, FeeDetails>))]
        List<IFeeDetails> FeeDetails { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPaybackTier, PaybackTier>))]
        List<IPaybackTier> PaybackTiers { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILoanScheduleDetails, LoanScheduleDetails>))]
        List<ILoanScheduleDetails> OriginalScheduleDetails { get; set; }
        string ModReason { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRemainderDetails, RemainderDetails>))]
        IRemainderDetails RemainderDetails { get; set; }
        ModStatus ModStatus { get; set; }
        int PaymentFailure { get; set; }
        string NextEscalationStage { get; set; }
        InterestAdjustmentType InterestAdjustmentType { get; set; }
        LoanModType LoanModType { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPaymentStream, PaymentStream>))]
        List<IPaymentStream> PaymentStreams { get; set; }
        double InterestToRecover { get; set; }
        int Term { get; set; }
        int? BillingDate { get; set; }
        int MaturityDays {get;set;}
        double MADPercentage { get; set; }

    }
}