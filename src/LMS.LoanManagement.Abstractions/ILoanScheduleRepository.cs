﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanScheduleRepository : IRepository<ILoanSchedule>
    {
        Task<List<ILoanSchedule>> GetLoanScheduleByLoanNumber(string loanNumber);
        Task<ILoanSchedule> GetLoanScheduleByLoanNumberAndScheduleVersion(string loanNumber, string scheduleVersion);
        Task<ILoanSchedule> GetLoanSchedule(string loanNumber, string version);
        Task<ILoanSchedule> GetLoanScheduleById(string loanNumber, string loanScheduleId);
        Task<ILoanSchedule> UpdateLoanSchedule(string loanNumber, string scheduleId, ILoanSchedule loanSchedule);
        Task<ILoanSchedule> UpdatePaidDetails(string loanNumber, ILoanSchedule loanSchedule);
        Task<List<ILoanSchedule>> GetLoanSchedules(List<string> loanNumbers);
    }
}