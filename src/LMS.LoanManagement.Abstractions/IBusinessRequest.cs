﻿
using System;
using System.Collections.Generic;


namespace LMS.LoanManagement.Abstractions
{
    public interface IBusinessRequest
    {
        IAddress Address { get; set; }
        string BusinessName { get; set; }
        string DBA { get; set; }
        IEmail Email { get; set; }
        string FedTaxId { get; set; }
        bool IsPrimary { get; set; }
        IPhone Phone { get; set; }
        DateTimeOffset EstablishedDate { get; set; }
        double BusinessMaxLimitAmount{get;set;}
         List<IIdentifier> BusinessIdentifiers{get;set;}
    }
}