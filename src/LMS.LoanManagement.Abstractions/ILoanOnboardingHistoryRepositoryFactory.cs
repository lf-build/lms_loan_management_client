﻿using LendFoundry.Security.Tokens;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanOnboardingHistoryRepositoryFactory
    {
        ILoanOnboardingHistoryRepository Create(ITokenReader reader);
    }
}