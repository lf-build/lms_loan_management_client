﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;

namespace LMS.LoanManagement.Abstractions
{
    public interface IBankDetails : IAggregate
    {
        string PaymentInstrumentId { get; set; }
        string AccountNumber { get; set; }
        AccountType AccountType { get; set; }
        string BankHolderName { get; set; }
        string Name { get; set; }
        string RoutingNumber { get; set; }
        string LoanNumber { get; set; }
        bool IsActive { get; set; }
        bool IsVerified { get; set; }
        string BankId { get; set; }
        List<IIdentifier> BranchIdentifiers { get; set; }
        
    }
}