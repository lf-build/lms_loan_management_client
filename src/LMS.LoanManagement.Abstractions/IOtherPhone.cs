﻿namespace LMS.LoanManagement.Abstractions
{
    public interface IOtherPhone
    {
        string Id { get; set; }
        bool Active { get; set; }
        string AvailabilityNotes { get; set; }
        string ContactPerson { get; set; }
        string PhoneNo { get; set; }
        string PhoneType { get; set; }
        string Relation { get; set; }
    }
}