﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public class Name : IName
    {
        public Name()
        {

        }
        public Name(IName name)
        {
            if (name == null)
                return;
            Id = Guid.NewGuid().ToString("N");
            First = name.First;
            Last = name.Last;
            Middle = name.Middle;
            Salutation = name.Salutation;
            Generation = name.Generation;
        }
        public string Id { get; set; }
        public string First { get; set; }
        public string Last { get; set; }
        public string Middle { get; set; }
        public string Salutation { get; set; }
        public string Generation { get; set; }
    }
}
