﻿namespace LMS.LoanManagement.Abstractions
{
    public interface IAutomaticFeeRequest
    {
        double PartialFeeAmount { get; set; }
        double OutStandingFeeAmount { get; set; }
        
    }
}