﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LMS.LoanManagement.Abstractions
{
    public class CollectionDetail : ICollectionDetail
    {
        public CollectionDetail()
        {
                
        }

        public CollectionDetail(ICollectionDetail collectionDetail)
        {
            if (collectionDetail == null)
                return;
            Id = Guid.NewGuid().ToString("N");
            Evaluation = collectionDetail.Evaluation;
            ReasonForNonPaymentOthers = collectionDetail.ReasonForNonPaymentOthers;
            SituationOthers = collectionDetail.SituationOthers;
            FactsOthers = collectionDetail.FactsOthers;
            EvaluationOthers = collectionDetail.EvaluationOthers;
            if (collectionDetail.ReasonForNonPayment != null && collectionDetail.ReasonForNonPayment.Any())
            {
                ReasonForNonPayment = new List<string>();
                ReasonForNonPayment.AddRange(collectionDetail.ReasonForNonPayment);
            }
            if (collectionDetail.Situation != null && collectionDetail.Situation.Any())
            {
                Situation = new List<string>();
                Situation.AddRange(collectionDetail.Situation);
            }
            if (collectionDetail.Facts != null && collectionDetail.Facts.Any())
            {
                Facts = new List<string>();
                Facts.AddRange(collectionDetail.Facts);
            }
        }

        public string Id { get; set; }

        public List<string> ReasonForNonPayment { get; set; }
        public string ReasonForNonPaymentOthers { get; set; }

        public List<string> Situation { get; set; }
        public string SituationOthers { get; set; }

        public List<string> Facts { get; set; }
        public string FactsOthers { get; set; }

        public string Evaluation { get; set; }
        public string EvaluationOthers { get; set; }
    }
}
