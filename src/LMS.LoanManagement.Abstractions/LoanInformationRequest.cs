﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public class LoanInformationRequest
    {
        public string LoanReferenceNumber { get; set; }
        public DateTimeOffset LoanFundedDate { get; set; }
        public DateTimeOffset LoanStartDate { get; set; }
        public DateTimeOffset LoanOnboardedDate { get; set; }
        public string LoanApplicationNumber { get; set; }
        public string ParentApplicationNumber { get; set; }
        public string LoanProductId { get; set; }
        public string PortFolioId { get; set; }
        public ProductCategory ProductCategory { get; set; }
        public ProductPortfolioType ProductPortfolioType { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPaymentInstrumentRequest, PaymentInstrumentRequest>))]
        public List<IPaymentInstrumentRequest> PaymentInstruments { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IApplicantRequest, ApplicantRequest>))]
        public List<IApplicantRequest> ApplicantRequest { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IBusinessRequest, BusinessRequest>))]
        public List<IBusinessRequest> BusinessRequest { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ILoanScheduleRequest, LoanScheduleRequest>))]
        public ILoanScheduleRequest LoanScheduleRequest { get; set; }
        public bool AutoInitialize { get; set; }
        public LoanType LoanType { get; set; }
        
        public double SettlementAmount { get; set; }
        public string FunderId { get; set; }
        
        public string AdditionalRefId{get;set;}  // This will be an alphanumeric ID to identify the end customer
        public string AggregatorName { get; set; } //It will be used while sending the funding information to Mintifi
        public string Beneficiary { get; set; } //This can be aggregator or the borrower

        public string BorrowerId { get; set; } //Unique Id of each Borrower


[JsonConverter (typeof (InterfaceListConverter<ILoanInvoiceDetails, LoanInvoiceDetails>))]
        public List<ILoanInvoiceDetails> InvoiceDetails { get; set; } //OnBoard Invoice DetaLoan

        public bool AutoAccrualForPastDate { get; set; }

    }
}
