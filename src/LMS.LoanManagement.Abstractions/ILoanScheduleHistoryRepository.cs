﻿using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanScheduleHistoryRepository : IRepository<ILoanScheduleHistory>
    {
        Task AddHistory(ILoanScheduleHistory loanInformationHistory);
    }
}