namespace LMS.LoanManagement.Abstractions
{
    public class ValidationResult
    {

        public bool Result {get;set;}
        public object Details {get;set;}
    }
}