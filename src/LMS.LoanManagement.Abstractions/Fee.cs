﻿namespace LMS.LoanManagement.Abstractions {
    public class Fee : IFee {
        public string Id { get; set; }
        public string FeeName { get; set; }
        public string FeeType { get; set; }
        public double FeeAmount { get; set; }
        public bool Applied { get; set; }
        public bool Accrue { get; set; }
        public double PaymentFrequencyAccrualAmount { get; set; }
        public double DailyFrequencyAccrualAmount { get; set; }
        public bool TaxApplicableFlag { get; set; }

    }
}