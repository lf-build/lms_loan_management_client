﻿using LendFoundry.Foundation.Date;

namespace LMS.LoanManagement.Abstractions
{
    public class AutoPayHistory: IAutoPayHistory
    {
       public  TimeBucket StopDate { get; set; }
       public TimeBucket ResumeDate { get; set; }
       public bool IsAutoPay { get; set; }
    }
}