﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public class ApplicantRequest : IApplicantRequest
    {
        public string PositionOfLoan { get; set; }
        public string Role { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IName, Name>))]
        public IName Name { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPII, PII>))]
        public IPII PII { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPhone, Phone>))]
        public List<IPhone> Phones { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IEmail, Email>))]
        public List<IEmail> Emails { get; set; }
        public bool IsPrimary { get; set; }
    }
}
