﻿using LendFoundry.ProductConfiguration;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public class ProductDetails
    {
        public Product Product { get; set; }
        public List<ProductParameterGroup> ProductParameterGroup { get; set; }
        public string ProductId { get; set; }
        public List<ProductParameters> ProductParameters { get; set; }
        public List<ProductFeeParameters> ProductFeeParameters { get; set; }
    }
}
