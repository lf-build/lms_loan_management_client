﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public class AutoPayRequest : IAutoPayRequest
    {
        public bool IsAutopay { get; set; }

        public DateTimeOffset? AutoPayStartDate { get; set; }

        public DateTimeOffset? AutoPayStopDate { get; set; }
        
        public AutoPayType AutoPayType{ get; set; }

      
    }

    
}
