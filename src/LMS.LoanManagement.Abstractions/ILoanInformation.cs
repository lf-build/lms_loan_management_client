﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;

namespace LMS.LoanManagement.Abstractions {
    public interface ILoanInformation : IAggregate {
        string LoanApplicationNumber { get; set; }
        TimeBucket LoanEndDate { get; set; }
        TimeBucket LoanFundedDate { get; set; }
        string LoanNumber { get; set; }
        TimeBucket LoanOnboardedDate { get; set; }
        string LoanProductId { get; set; }
        TimeBucket LoanStartDate { get; set; }
        string PortFolioId { get; set; }
        ProductCategory ProductCategory { get; set; }
        ProductPortfolioType ProductPortfolioType { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IPaymentInstrument, PaymentInstrument>))]
        List<IPaymentInstrument> PaymentInstruments { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IApplicantDetails, ApplicantDetails>))]
        IApplicantDetails PrimaryApplicantDetails { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IBusinessDetails, BusinessDetails>))]
        IBusinessDetails PrimaryBusinessDetails { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IApplicantDetails, ApplicantDetails>))]
        List<IApplicantDetails> ApplicantDetails { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IBusinessDetails, BusinessDetails>))]
        List<IBusinessDetails> BusinessDetails { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IDrawDownDetails, DrawDownDetails>))]
        List<IDrawDownDetails> DrawDownDetails { get; set; }
        Currency Currency { get; set; }
        bool IsAutoPay { get; set; }
        TimeBucket AutoPayStartDate { get; set; }
        TimeBucket AutoPayStopDate { get; set; }
        AutoPayType AutoPayType { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IAutoPayHistory, AutoPayHistory>))]
        List<IAutoPayHistory> AutoPayHistory { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IOtherContact, OtherContact>))]
        List<IOtherContact> OtherContacts { get; set; }
        string CollectionStage { get; set; }
        LoanType LoanType { get; set; }
        string IsRenewable { get; set; }
        string ParentLoanNumber { get; set; }
        string OriginalParentLoanNumber { get; set; }
        int SequenceNumber { get; set; }
        string FunderId { get; set; }
        string MandateNumber{get;set;} 
        string MandateType{get;set;} 
        string AdditionalRefId{get;set;}
        string AggregatorName { get; set; } 
        string Beneficiary { get; set; } 
        string LoanReferenceNumber{get;set;}
        string BorrowerId { get; set; }
       
        [JsonConverter (typeof (InterfaceListConverter<ILoanInvoiceDetails, LoanInvoiceDetails>))]
         List<ILoanInvoiceDetails> InvoiceDetails { get; set; }


    }
}