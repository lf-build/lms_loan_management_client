﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public class PaymentInstrument : IPaymentInstrument
    {
        public PaymentInstrument()
        {

        }
        public PaymentInstrument(IPaymentInstrumentRequest paymentInstrumentRequest)
        {
            if (paymentInstrumentRequest == null)
                return;
            Id = Guid.NewGuid().ToString("N");
            PaymentInstrumentType = paymentInstrumentRequest.PaymentInstrumentType;
            IsActive = paymentInstrumentRequest.IsActive;
            MandateNumber=paymentInstrumentRequest.MandateNumber;
            MandateType=paymentInstrumentRequest.MandateType;
        }

        public string Id { get; set; }
        public PaymentInstrumentType PaymentInstrumentType { get; set; }
        public bool IsActive { get; set; }
        public string MandateNumber{get;set;} 
        public string MandateType{get;set;} 
    }
}
