﻿using System;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public interface IPII
    {
        string Id { get; set; }
        DateTime DOB { get; set; }
        string SSN { get; set; }
        List<IIdentifier> PersonalIdentifiers{get;set;}
        
    }
}