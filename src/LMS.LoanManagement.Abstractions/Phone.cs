﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public class Phone : IPhone
    {
        public Phone()
        {

        }
        public Phone(IPhone phone)
        {
            if (phone == null)
                return;
            Id = Guid.NewGuid().ToString("N");
            PhoneNo = phone.PhoneNo;
            PhoneType = phone.PhoneType;
            IsActive = phone.IsActive;
        }
        public string Id { get; set; }
        public string PhoneNo { get; set; }
        public PhoneType PhoneType { get; set; }
        public bool IsActive { get; set; }
    }
}
