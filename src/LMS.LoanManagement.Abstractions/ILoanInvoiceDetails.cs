using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LMS.Foundation.Amortization;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;


namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanInvoiceDetails
    {
        string Id { get; set; }
        /// <summary>
        /// Unique ID for the invoice. 
        /// </summary>
        string InvoiceID {get;set;}
        /// <summary>
        /// The organization / person that has raised the invoice (requires to be paid)
        /// Also known as Anchor
        /// It is expected that Invoice ID + Issuer Name + Date of Invoice will be unique
        /// However no such check is enforced
        /// </summary>
        string IssuerName { get; set; }
        /// <summary>
        /// The days till which the issuer is ready to wait to be paid
        /// TODO : to be validated if this is true
        /// </summary>
        int? CreditDaysOfInvoice { get; set; }
        /// <summary>
        /// amount of the invoice.
        /// Loans backed by this invoice will generally be equal to or less than this amout
        /// </summary>
        double? InvoiceAmount { get; set; }
        /// <summary>
        /// Date when the invoice was raised
        /// </summary>
        DateTimeOffset? DateOfInvoice { get; set; }
        /// <summary>
        /// Date when the invoice is due
        /// TODO : how is it related to credit days of invoice and why are we capturing both?
        /// </summary>
        DateTimeOffset? DueDateOfInvoice { get; set; }
    }
}
