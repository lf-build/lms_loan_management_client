﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json;

namespace LMS.LoanManagement.Abstractions {
    public interface IBusinessDetails {
        string Id { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IAddress, Address>))]
        IAddress Address { get; set; }
        string BusinessName { get; set; }
        string DBA { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IEmail, Email>))]
        IEmail Email { get; set; }
        string FedTaxId { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IPhone, Phone>))]
        IPhone Phone { get; set; }
        TimeBucket EstablishedDate { get; set; }
        double BusinessMaxLimitAmount{get;set;}

        [JsonConverter (typeof (InterfaceListConverter<IIdentifier, Identifier>))]
        List<IIdentifier> BusinessIdentifiers { get; set; }

    }
}