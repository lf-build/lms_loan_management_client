﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public interface IDrawDownDetails
    {
        DateTime DrawDownDate { get; set; }
        string DrawDownNumber { get; set; }
        double LoanAmount { get; set; }
        string LoanScheduleNumber { get; set; }
        double TotalDrawDownAmount { get; set; }
    }
}