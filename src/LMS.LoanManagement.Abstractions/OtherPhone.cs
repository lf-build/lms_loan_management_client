﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public class OtherPhone : IOtherPhone
    {
        public OtherPhone()
        {

        }
        public OtherPhone(IOtherPhone phone)
        {
            if (phone == null)
                return;
            Id = Guid.NewGuid().ToString("N");
            PhoneType = phone.PhoneType;
            PhoneNo = phone.PhoneNo;
            ContactPerson = phone.ContactPerson;
            Relation = phone.Relation;
            AvailabilityNotes = phone.AvailabilityNotes;
            Active = phone.Active;
        }
        public string Id { get; set; }
        public string PhoneType { get; set; }
        public string PhoneNo { get; set; }
        public string ContactPerson { get; set; }
        public string Relation { get; set; }
        public string AvailabilityNotes { get; set; }
        public bool Active { get; set; }
    }
}
