﻿using System;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public interface IFeeDetails
    {
        string Id { get; set; }
        bool AutoSchedulePayment { get; set; }
        DateTimeOffset DateInitiated { get; set; }
        List<string> Events { get; set; }
        double FeeAmount { get; set; }
        DateTimeOffset FeeDueDate { get; set; }
        string FeeId { get; set; }
        string FeeType { get; set; }
        bool IsProcessed { get; set; }
        string FeeName { get; set; }
    }
}