﻿using LendFoundry.Security.Tokens;

namespace LMS.LoanManagement.Abstractions
{
    public interface IPayOffFeeDiscountRepositoryFactory
    {
        IPayOffFeeDiscountRepository Create(ITokenReader reader);
    }
}