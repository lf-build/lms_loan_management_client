﻿namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanOnboardingListener
    {
        void Start();
    }
}
