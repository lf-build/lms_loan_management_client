﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public class OtherEmail : IOtherEmail
    {
        public OtherEmail()
        {

        }
        public OtherEmail(IOtherEmail email)
        {
            if (email == null)
                return;
            Id = Guid.NewGuid().ToString("N");
            EmailAddress = email.EmailAddress;
            EmailType = email.EmailType;
            ContactPerson = email.ContactPerson;
            Relation = email.Relation;
            Active = email.Active;
        }
        public string Id { get; set; }
        public string EmailType { get; set; }
        public string EmailAddress { get; set; }
        public string ContactPerson { get; set; }
        public string Relation { get; set; }
        public bool Active { get; set; }
    }
}
