﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public interface IPaymentScheduleRequest
    {
        int InstallmentNumber { get; set; }
        DateTimeOffset ScheduleDate { get; set; }
    }
}