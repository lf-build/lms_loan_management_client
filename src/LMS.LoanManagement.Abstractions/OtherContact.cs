﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LMS.LoanManagement.Abstractions
{
    public class OtherContact : IOtherContact
    {
        public OtherContact()
        {

        }
        public OtherContact(IOtherContact otherContact)
        {
            if (otherContact != null)
            {
                Id = Guid.NewGuid().ToString("N");
                if (otherContact.Address != null && otherContact.Address.Any())
                {
                    Address = new List<IOtherAddress>();
                    otherContact.Address.ForEach(a => a.Id = Guid.NewGuid().ToString("N"));
                    Address.AddRange(otherContact.Address);
                }
                if (otherContact.Email != null && otherContact.Email.Any())
                {
                    Email = new List<IOtherEmail>();
                    otherContact.Email.ForEach(a => a.Id = Guid.NewGuid().ToString("N"));
                    Email.AddRange(otherContact.Email);
                }
                if (otherContact.Phone != null && otherContact.Phone.Any())
                {
                    Phone = new List<IOtherPhone>();
                    otherContact.Phone.ForEach(a => a.Id = Guid.NewGuid().ToString("N"));
                    Phone.AddRange(otherContact.Phone);
                }
                if (otherContact.CollectionDetails != null && otherContact.CollectionDetails.Any())
                {
                    CollectionDetails = new List<ICollectionDetail>();
                    otherContact.CollectionDetails.ForEach(a => a.Id = Guid.NewGuid().ToString("N"));
                    CollectionDetails.AddRange(otherContact.CollectionDetails);
                }
            }
        }

        public string Id { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IOtherAddress, OtherAddress>))]
        public List<IOtherAddress> Address { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IOtherEmail, OtherEmail>))]
        public List<IOtherEmail> Email { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IOtherPhone, OtherPhone>))]
        public List<IOtherPhone> Phone { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ICollectionDetail, CollectionDetail>))]
        public List<ICollectionDetail> CollectionDetails { get; set; }
    }
}
