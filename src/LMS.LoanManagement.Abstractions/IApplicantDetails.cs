﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public interface IApplicantDetails
    {
        string Id { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        List<IAddress> Addresses { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IEmail, Email>))]
        List<IEmail> Emails { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IName, Name>))]
        IName Name { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPhone, Phone>))]
        List<IPhone> Phones { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPII, PII>))]
        IPII PII { get; set; }
        string PositionOfLoan { get; set; }
        string Role { get; set; }
    }
}