﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;


namespace LMS.LoanManagement.Abstractions
{
    public class PII : IPII
    {
        public PII()
        {

        }
        public PII(IPII pii)
        {
            if (pii == null)
                return;
            Id = Guid.NewGuid().ToString("N");
            DOB = pii.DOB;
            SSN = pii.SSN;
            PersonalIdentifiers=pii.PersonalIdentifiers != null ?pii.PersonalIdentifiers.Select(i=> new Identifier(i)).ToList<IIdentifier>(): null            ;
        }
        public string Id { get; set; }
        public DateTime DOB { get; set; }
        public string SSN { get; set; }
        
        [JsonConverter(typeof(InterfaceListConverter<IIdentifier, Identifier>))]
        public List<IIdentifier> PersonalIdentifiers{get;set;}
    }
}
