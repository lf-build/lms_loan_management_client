﻿using LendFoundry.Security.Tokens;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanScheduleRepositoryFactory
    {
        ILoanScheduleRepository Create(ITokenReader reader);
    }
}