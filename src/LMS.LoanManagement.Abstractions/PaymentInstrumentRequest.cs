﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LMS.LoanManagement.Abstractions {
    public class PaymentInstrumentRequest : IPaymentInstrumentRequest {
        public string Id { get; set; }
        public PaymentInstrumentType PaymentInstrumentType { get; set; }
        public bool IsActive { get; set; }
        public string MandateNumber { get; set; } //It requires when PaymentInstrumentType is ACH
        public string MandateType { get; set; } //It requires when PaymentInstrumentType is ACH
        [JsonConverter (typeof (InterfaceConverter<IBankDetails, BankDetails>))]
        public IBankDetails BankDetails { get; set; }
    }
}