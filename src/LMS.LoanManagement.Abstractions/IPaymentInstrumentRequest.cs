﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LMS.LoanManagement.Abstractions
{
    public interface IPaymentInstrumentRequest
    {
        string Id { get; set; }
        bool IsActive { get; set; }
        string MandateNumber{get;set;} 
        string MandateType{get;set;} 
        PaymentInstrumentType PaymentInstrumentType { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IBankDetails, BankDetails>))]
        IBankDetails BankDetails { get; set; }
    }
}