﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Abstractions
{
    public class PayOffResponse
    {
        public double AdditionalInterest { get; set; }
        public double Interest { get; set; }
        public DateTime PayOffDate { get; set; }
        public double Principal { get; set; }
        public double TotalAmount { get; set; }
    }
}
