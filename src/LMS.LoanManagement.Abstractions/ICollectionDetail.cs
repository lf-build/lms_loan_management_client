﻿using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public interface ICollectionDetail
    {
        string Id { get; set; }
        string Evaluation { get; set; }
        string EvaluationOthers { get; set; }
        List<string> Facts { get; set; }
        string FactsOthers { get; set; }
        List<string> ReasonForNonPayment { get; set; }
        string ReasonForNonPaymentOthers { get; set; }
        List<string> Situation { get; set; }
        string SituationOthers { get; set; }
    }
}