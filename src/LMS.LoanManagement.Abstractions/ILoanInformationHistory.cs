﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanInformationHistory : IAggregate,ILoanInformation
    {
        DateTime UpdatedDateTime { get; set; }
        string UpdatedBy { get; set; }
    }
}