﻿using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LMS.Foundation.Amortization;
using LMS.Foundation.Calculus;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public class LoanSchedule : Aggregate, ILoanSchedule
    {
        public LoanSchedule()
        {

        }

        public LoanSchedule(ILoanScheduleRequest loanScheduleRequest)
        {
            if (loanScheduleRequest == null)
                return;
            LoanNumber = loanScheduleRequest.LoanNumber;
            ScheduleVersionNo = loanScheduleRequest.ScheduleVersionNo;
            ScheduledCreatedReason = loanScheduleRequest.ScheduledCreatedReason;
            ScheduleNotes = loanScheduleRequest.ScheduleNotes;
            LoanAmount = loanScheduleRequest.LoanAmount;
            FundedAmount = loanScheduleRequest.FundedAmount;
            FundedDate = new TimeBucket(loanScheduleRequest.FundedDate);
            FirstPaymentDate = new TimeBucket(loanScheduleRequest.FirstPaymentDate.Value);
            PaymentFrequency = loanScheduleRequest.PaymentFrequency;
            AnnualRate = loanScheduleRequest.AnnualRate;
            Tenure = loanScheduleRequest.Tenure;
            MaturityDays =loanScheduleRequest.Tenure * 30 ;
            FactorRate = loanScheduleRequest.FactorRate;
            Term=loanScheduleRequest.Term;
            PaymentFrequencyRate = loanScheduleRequest.PaymentFrequencyRate == 0 ? InterestRate.Calculate_InterestRate(loanScheduleRequest.PaymentFrequency.ToString(), loanScheduleRequest.AnnualRate) : loanScheduleRequest.PaymentFrequencyRate;
            DailyRate = loanScheduleRequest.DailyRate == 0
                        ? (loanScheduleRequest.PaymentFrequency == Foundation.Amortization.PaymentFrequency.Monthly
                            ? (PaymentFrequencyRate / 30)
                            : loanScheduleRequest.PaymentFrequency == Foundation.Amortization.PaymentFrequency.Weekly
                                ? (PaymentFrequencyRate / 7)
                                : loanScheduleRequest.PaymentFrequency == Foundation.Amortization.PaymentFrequency.BiWeekly
                                    ? (PaymentFrequencyRate / 14)
                                    : PaymentFrequencyRate)
                        : loanScheduleRequest.DailyRate;
            CreditLimit = loanScheduleRequest.CreditLimit;
            if (loanScheduleRequest.Fees != null)
            {
                Fees = new List<IFee>();
                Fees.AddRange(loanScheduleRequest.Fees);
            }
            if (loanScheduleRequest.PaybackTiers != null)
            {
                PaybackTiers = new List<IPaybackTier>();
                PaybackTiers.AddRange(loanScheduleRequest.PaybackTiers);
            }
            ModStatus = ModStatus.None; // By default set ModStatus as none
            InterestAdjustmentType = loanScheduleRequest.InterestAdjustmentType;
            if (loanScheduleRequest.PaymentStreams != null)
            {
                PaymentStreams = new List<IPaymentStream>();
                PaymentStreams.AddRange(loanScheduleRequest.PaymentStreams);
            }
            BillingDate = loanScheduleRequest.BillingDate;
            MADPercentage=loanScheduleRequest.MADPercentage;
        }

        public string LoanScheduleNumber { get; set; }
        public string LoanNumber { get; set; }
        public string ScheduleVersionNo { get; set; }
        public TimeBucket ScheduleVersionDate { get; set; }
        public string ScheduledCreatedReason { get; set; }
        public string ScheduleNotes { get; set; }
        public double LoanAmount { get; set; }
        public double FundedAmount { get; set; }
        public TimeBucket FundedDate { get; set; }
        public TimeBucket FirstPaymentDate { get; set; }
        public PaymentFrequency PaymentFrequency { get; set; }
        public double PaymentFrequencyRate { get; set; }
        public double DailyRate { get; set; }
        public double AnnualRate { get; set; }
        public int Tenure { get; set; }
        public double FactorRate { get; set; }
        public bool IsCurrent { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILoanScheduleDetails, LoanScheduleDetails>))]
        public List<ILoanScheduleDetails> ScheduleDetails { get; set; }
        public double CreditLimit { get; set; }
        public double TotalDrawDown { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IFee, Fee>))]
        public List<IFee> Fees { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IFeeDetails, FeeDetails>))]
        public List<IFeeDetails> FeeDetails { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPaybackTier, PaybackTier>))]
        public List<IPaybackTier> PaybackTiers { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILoanScheduleDetail, LoanScheduleDetail>))]
        public List<ILoanScheduleDetail> DailyScheduleDetails { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILoanScheduleDetails, LoanScheduleDetails>))]
        public List<ILoanScheduleDetails> OriginalScheduleDetails { get; set; }
        public string ModReason { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRemainderDetails, RemainderDetails>))]
        public IRemainderDetails RemainderDetails { get; set; }
        public ModStatus ModStatus { get; set; }
        public int PaymentFailure { get; set; }
        public string NextEscalationStage { get; set; }
        public InterestAdjustmentType InterestAdjustmentType { get; set; }
        public LoanModType LoanModType { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPaymentStream, PaymentStream>))]
        public List<IPaymentStream> PaymentStreams { get; set; }
        public double InterestToRecover { get; set; }
        public  int Term{ get; set; }
        public int? BillingDate { get; set; }
        public int MaturityDays {get;set;}
        public double MADPercentage { get; set; }

    }
        
    
}
