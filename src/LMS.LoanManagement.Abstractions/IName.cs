﻿namespace LMS.LoanManagement.Abstractions
{
    public interface IName
    {
        string Id { get; set; }
        string First { get; set; }
        string Generation { get; set; }
        string Last { get; set; }
        string Middle { get; set; }
        string Salutation { get; set; }
    }
}