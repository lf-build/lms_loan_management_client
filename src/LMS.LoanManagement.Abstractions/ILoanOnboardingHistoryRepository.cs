﻿using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanOnboardingHistoryRepository : IRepository<ILoanInformationHistory>
    {
        Task AddHistory(ILoanInformationHistory loanInformationHistory);
    }
}