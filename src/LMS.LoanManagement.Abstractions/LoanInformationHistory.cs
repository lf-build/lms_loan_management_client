﻿using System;

namespace LMS.LoanManagement.Abstractions
{

    public class LoanInformationHistory : LoanInformation, ILoanInformationHistory
    {
        public LoanInformationHistory(ILoanInformation loanInformation)
        {
            if (loanInformation == null)
                return;
            LoanNumber = loanInformation.LoanNumber;
            LoanOnboardedDate = loanInformation.LoanOnboardedDate;
            LoanFundedDate = loanInformation.LoanFundedDate;
            LoanStartDate = loanInformation.LoanStartDate;
            LoanEndDate = loanInformation.LoanEndDate;
            LoanApplicationNumber = loanInformation.LoanApplicationNumber;
            LoanProductId = loanInformation.LoanProductId;
            ProductCategory = loanInformation.ProductCategory;
            ProductPortfolioType = loanInformation.ProductPortfolioType;
            PrimaryApplicantDetails = loanInformation.PrimaryApplicantDetails;
            PrimaryBusinessDetails = loanInformation.PrimaryBusinessDetails;
            AutoPayStartDate = loanInformation.AutoPayStartDate;
            AutoPayStopDate = loanInformation.AutoPayStopDate;
            AutoPayHistory = loanInformation.AutoPayHistory;
            IsAutoPay = loanInformation.IsAutoPay;
            Currency = loanInformation.Currency;
            PortFolioId = loanInformation.PortFolioId;
            CollectionStage = loanInformation.CollectionStage;
            if (loanInformation.ApplicantDetails!= null)
            {
                ApplicantDetails = new System.Collections.Generic.List<IApplicantDetails>();
                ApplicantDetails.AddRange(loanInformation.ApplicantDetails);
            }
            if(loanInformation.BusinessDetails != null)
            {
                BusinessDetails = new System.Collections.Generic.List<IBusinessDetails>();
                BusinessDetails.AddRange(loanInformation.BusinessDetails);
            }
            if(loanInformation.DrawDownDetails != null)
            {
                DrawDownDetails = new System.Collections.Generic.List<IDrawDownDetails>();
                DrawDownDetails.AddRange(loanInformation.DrawDownDetails);
            }
            if(loanInformation.OtherContacts != null)
            {
                OtherContacts = new System.Collections.Generic.List<IOtherContact>();
                OtherContacts.AddRange(loanInformation.OtherContacts);
            }
            if(loanInformation.PaymentInstruments != null)
            {
                PaymentInstruments = new System.Collections.Generic.List<IPaymentInstrument>();
                PaymentInstruments.AddRange(loanInformation.PaymentInstruments);
            }
            
        }
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDateTime { get; set; }
    }
}
