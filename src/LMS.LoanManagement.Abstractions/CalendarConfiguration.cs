﻿using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public class CalendarConfiguration
    {
        public Dictionary<string, string> Dates { get; set; }
    }
}
