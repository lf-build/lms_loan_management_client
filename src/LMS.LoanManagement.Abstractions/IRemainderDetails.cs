﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public interface IRemainderDetails
    {
        DateTimeOffset AsOfDate { get; set; }
        double RemainderAmount { get; set; }
        double RemainderFee { get; set; }
        double RemainderInterest { get; set; }
        double RemainderPrincipal { get; set; }
    }
}