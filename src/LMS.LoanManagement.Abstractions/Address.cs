﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public class Address : IAddress
    {
        public Address()
        {

        }
        public Address(IAddress address)
        {
            if (address == null)
                return;
            Id = Guid.NewGuid().ToString("N");
            AddressType = address.AddressType;
            AddressLine1 = address.AddressLine1;
            AddressLine2 = address.AddressLine2;
            City = address.City;
            State = address.State;
            Zip = address.Zip;
            IsActive = address.IsActive;
        }
        public string Id { get; set; }
        public AddressType AddressType { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public bool IsActive { get; set; }
    }
}
