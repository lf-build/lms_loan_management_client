﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LMS.LoanManagement.Abstractions
{
    public class BankDetailsHistory : Aggregate, IBankDetailsHistory
    {
        public string Name { get; set; }
        public string AccountNumber { get; set; }
        public string RoutingNumber { get; set; }
        public string BankHolderName { get; set; }
        public AccountType AccountType { get; set; }
        public string LoanNumber { get; set; }
        public bool IsActive { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public string UpdatedBy { get; set; }
        public string BankId { get; set; }

    }
}
