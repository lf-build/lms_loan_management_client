﻿using LendFoundry.Security.Tokens;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanScheduleHistoryRepositoryFactory
    {
        ILoanScheduleHistoryRepository Create(ITokenReader reader);
    }
}