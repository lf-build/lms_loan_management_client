﻿using LendFoundry.Foundation.Client;
using LMS.Foundation.Amortization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public class LoanScheduleRequest : ILoanScheduleRequest
    {
        public string LoanNumber { get; set; }
        public string ScheduleVersionNo { get; set; }
        public string ScheduledCreatedReason { get; set; }
        public string ScheduleNotes { get; set; }
        public double LoanAmount { get; set; }
        public double FundedAmount { get; set; }
        public DateTimeOffset FundedDate { get; set; }
        public DateTimeOffset? FirstPaymentDate { get; set; }
        public DateTimeOffset OriginalFirstPaymentDate { get; set; }
        public DateTimeOffset? ExpectedFirstPaymentDate { get; set; }
        public PaymentFrequency PaymentFrequency { get; set; }
        public double PaymentFrequencyRate { get; set; }
        public double DailyRate { get; set; }
        public double AnnualRate { get; set; }
        public int Tenure { get; set; }
        public double FactorRate { get; set; }
        public double CreditLimit { get; set; }
        public string DrawDownNumber { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IFee, Fee>))]
        public List<IFee> Fees { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPaybackTier, PaybackTier>))]
        public List<IPaybackTier> PaybackTiers { get; set; }
        public InterestAdjustmentType InterestAdjustmentType { get; set; }
        public InterestHandlingType InterestHandlingType { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPaymentStream, PaymentStream>))]
        public List<PaymentStream> PaymentStreams { get; set; }
        public List<SlabDetails> SlabDetails { get; set; }
        public  int Term { get; set; } //It is use for capture tenure of the loan in months 
        public  int NumberOfDaysInMonth { get; set; }
        public int NumberOfDaysInWeek { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ILoanScheduleDetails, LoanScheduleDetails>))]
        public List<ILoanScheduleDetails> LoanScheduleDetails { get; set; }
        public bool IsMigration { get; set; }
        public int? BillingDate { get; set; }
        /// <summary>
        /// If set to InterestOnlyFollowedByBalloon, interest only schedule will be generated
        /// with a final repayment of principal in the last instalment
        /// </summary>
        public ScheduleType ScheduleType { get; set; }
        public double MADPercentage { get; set; }
        public double MinimumAmountDue { get; set; }


    }
}
