﻿namespace LMS.LoanManagement.Abstractions
{
    public interface IEmail
    {
        string Id { get; set; }
        string EmailAddress { get; set; }
        EmailType EmailType { get; set; }
        bool IsActive { get; set; }
    }
}