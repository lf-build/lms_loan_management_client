﻿namespace LMS.LoanManagement.Abstractions
{
    public interface IAddress
    {
        string Id { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        AddressType AddressType { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Zip { get; set; }
        bool IsActive { get; set; }
    }
}