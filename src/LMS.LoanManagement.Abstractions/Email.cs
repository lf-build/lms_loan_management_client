﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public class Email : IEmail
    {
        public Email()
        {

        }
        public Email(IEmail email)
        {
            if (email == null)
                return;
            Id = Guid.NewGuid().ToString("N");
            EmailAddress = email.EmailAddress;
            EmailType = email.EmailType;
            IsActive = email.IsActive;
        }
        public string Id { get; set; }
        public string EmailAddress { get; set; }
        public EmailType EmailType { get; set; }
        public bool IsActive { get; set; }
    }
}
