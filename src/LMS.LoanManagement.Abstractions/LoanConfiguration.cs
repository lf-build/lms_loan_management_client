﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Identity;
using LMS.Foundation.Amortization;

namespace LMS.LoanManagement.Abstractions 
{
    public class LoanConfiguration : IDependencyConfiguration
    {
        public string DefaultStatusFlow { get; set; }
        public CaseInsensitiveDictionary<string> Statuses { get; set; }
        public string Currency { get; set; }
        public EventMapping[] Events { get; set; }
        public bool IsCashBalanceLinked { get; set; }
        public bool ScheduleHolidays { get; set; }
        public bool IsDoubleScheduleForHoliday { get; set; }
        public InterestHandlingType InterestHandlingType { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
        public string FirstPaymentDateRuleName { get; set; }
        public CaseInsensitiveDictionary<string> ValidationRules { get; set; }
        public bool PrimaryBusinessMaxLimit { get; set; }
        public string ScheduleReamortizeReason { get; set; }
        // TODO: Temporary configuration to use latest amortization method.
        public bool UseLatestAmortization { get; set; }
    }
}