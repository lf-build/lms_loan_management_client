namespace LMS.LoanManagement.Abstractions
{
    public class UnwindRequest: IUnwindRequest
    {
        public bool UnwindFeesAtOrigination { get; set; }
    }
}