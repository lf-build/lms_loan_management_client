﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;

namespace LMS.LoanManagement.Abstractions {

    public class LoanInformation : Aggregate, ILoanInformation {
        public string LoanNumber { get; set; }
        public TimeBucket LoanOnboardedDate { get; set; }
        public TimeBucket LoanFundedDate { get; set; }
        public TimeBucket LoanStartDate { get; set; }
        public TimeBucket LoanEndDate { get; set; }
        public string LoanApplicationNumber { get; set; }
        public string LoanProductId { get; set; }
        public string PortFolioId { get; set; }
        public ProductCategory ProductCategory { get; set; }
        public ProductPortfolioType ProductPortfolioType { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IPaymentInstrument, PaymentInstrument>))]
        public List<IPaymentInstrument> PaymentInstruments { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IApplicantDetails, ApplicantDetails>))]
        public IApplicantDetails PrimaryApplicantDetails { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IBusinessDetails, BusinessDetails>))]
        public IBusinessDetails PrimaryBusinessDetails { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IApplicantDetails, ApplicantDetails>))]
        public List<IApplicantDetails> ApplicantDetails { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IBusinessDetails, BusinessDetails>))]
        public List<IBusinessDetails> BusinessDetails { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IDrawDownDetails, DrawDownDetails>))]
        public List<IDrawDownDetails> DrawDownDetails { get; set; }
        public Currency Currency { get; set; }
        public bool IsAutoPay { get; set; }
        public TimeBucket AutoPayStartDate { get; set; }
        public TimeBucket AutoPayStopDate { get; set; }
        public AutoPayType AutoPayType { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IAutoPayHistory, AutoPayHistory>))]
        public List<IAutoPayHistory> AutoPayHistory { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IOtherContact, OtherContact>))]
        public List<IOtherContact> OtherContacts { get; set; }
        public string CollectionStage { get; set; }
        public LoanType LoanType { get; set; }
        public string IsRenewable { get; set; }
        public string ParentLoanNumber { get; set; }
        public string OriginalParentLoanNumber { get; set; }
        public int SequenceNumber { get; set; }
        public string FunderId { get; set; }
        public string MandateNumber{get;set;} 
        public string MandateType{get;set;} 
        public string AdditionalRefId{get;set;}
        public string AggregatorName { get; set; } 
        public string Beneficiary { get; set; } 
        public string LoanReferenceNumber { get; set; }
        public string BorrowerId { get; set; }
   
   [JsonConverter (typeof (InterfaceListConverter<ILoanInvoiceDetails, LoanInvoiceDetails>))]
        public List<ILoanInvoiceDetails> InvoiceDetails { get; set; }


    }
}