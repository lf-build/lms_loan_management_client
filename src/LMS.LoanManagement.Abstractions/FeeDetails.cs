﻿using System;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public class FeeDetails : IFeeDetails
    {
        public string Id { get; set; }
        public string FeeId { get; set; }
        public string FeeType { get; set; }
        public double FeeAmount { get; set; }
        public DateTimeOffset FeeDueDate { get; set; }
        public bool AutoSchedulePayment { get; set; }
        public List<string> Events { get; set; }
        public DateTimeOffset DateInitiated { get; set; }
        public bool IsProcessed { get; set; }
        public string FeeName { get; set; }
    }
}
