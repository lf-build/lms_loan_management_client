﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanModificationActivationRequest
    {
        DateTimeOffset? ActivationDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ILoanModificationRequest, LoanModificationRequest>))]
        ILoanModificationRequest LoanModificationRequest { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILoanScheduleDetails, LoanScheduleDetails>))]
        List<ILoanScheduleDetails> LoanScheduleDetails { get; set; }
        string ScheduledCreatedReason { get; set; }
        string ModReason { get; set; }
        string ScheduleNotes { get; set; }
        string CollectionStage { get; set; }
        int PaymentFailure { get; set; }
        string NextEscalationStage { get; set; }
    }
}