﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LMS.LoanManagement.Abstractions
{
    public interface IBankDetailsHistory : IAggregate
    {
        string AccountNumber { get; set; }
        AccountType AccountType { get; set; }
        string BankHolderName { get; set; }
        string Name { get; set; }
        string RoutingNumber { get; set; }
        string LoanNumber { get; set; }
        bool IsActive { get; set; }
        DateTime UpdatedDateTime { get; set; }
        string UpdatedBy { get; set; }
        string BankId { get; set; }
    }
}