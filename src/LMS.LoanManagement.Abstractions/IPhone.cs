﻿namespace LMS.LoanManagement.Abstractions
{
    public interface IPhone
    {
        string Id { get; set; }
        string PhoneNo { get; set; }
        PhoneType PhoneType { get; set; }
        bool IsActive { get; set; }
    }
}