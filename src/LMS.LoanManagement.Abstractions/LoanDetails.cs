﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Abstractions
{
    public class LoanDetails : LoanInformation, ILoanDetails
    {
        public List<IBankDetails> BankDetails { get; set; }
        public List<ILoanSchedule> LoanSchedules { get; set; }

    }
}
