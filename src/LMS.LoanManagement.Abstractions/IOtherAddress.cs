﻿namespace LMS.LoanManagement.Abstractions
{
    public interface IOtherAddress
    {
        string Id { get; set; }
        bool Active { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string AddressType { get; set; }
        string AvailabilityNotes { get; set; }
        string City { get; set; }
        string ContactPerson { get; set; }
        string Relation { get; set; }
        string State { get; set; }
        string Zip { get; set; }
    }
}