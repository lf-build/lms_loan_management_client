﻿namespace LMS.LoanManagement.Abstractions
{
    public interface IOtherEmail
    {
        string Id { get; set; }
        bool Active { get; set; }
        string ContactPerson { get; set; }
        string EmailAddress { get; set; }
        string EmailType { get; set; }
        string Relation { get; set; }
    }
}