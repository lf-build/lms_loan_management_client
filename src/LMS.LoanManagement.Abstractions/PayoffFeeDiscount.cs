﻿using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public class PayOffFeeDiscount : Aggregate, IPayOffFeeDiscount
    {
        public string LoanNumber { get; set; }
      

        public string FeeDiscountId { get; set; }

        public string FeeType { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPayOffFeeScheduleDetails, PayOffFeeScheduleDetails>))]
        public List<IPayOffFeeScheduleDetails> PayOffFeeScheduleDetails { get; set; }
    }
}
