﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Abstractions
{
    public class RemainderDetails : IRemainderDetails
    {
        public double RemainderAmount { get; set; }
        public double RemainderPrincipal { get; set; }
        public double RemainderInterest { get; set; }
        public double RemainderFee { get; set; }
        public DateTimeOffset AsOfDate { get; set; }
    }
}
