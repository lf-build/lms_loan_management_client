﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public class LoanModificationActivationRequest : ILoanModificationActivationRequest
    {
        public DateTimeOffset? ActivationDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ILoanModificationRequest, LoanModificationRequest>))]
        public ILoanModificationRequest LoanModificationRequest { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILoanScheduleDetails, LoanScheduleDetails>))]
        public List<ILoanScheduleDetails> LoanScheduleDetails { get; set; }
        public string ScheduledCreatedReason { get; set; }
        public string ModReason { get; set; }
        public string ScheduleNotes { get; set; }
        public string CollectionStage { get; set; }
        public int PaymentFailure { get; set; }
        public string NextEscalationStage { get; set; }
    }
}
