﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanOnboardingService
    {
        Task<ILoanInformation> GetLoanInformationByLoanNumber(string loanNumber);
        Task<ILoanInformation> GetLoanInformationByApplicationNumber(string applicationNumber);
        Task<ILoanInformation> OnBoard(LoanInformationRequest loanInformationRequest);
        Task<ILoanInformation> UpdateLoanInformation(string loanNumber, LoanInformationRequest loanInformationRequest);
        Task<ILoanInformation> UpdateAddress(string type, string loanNumber, string id, IAddress address);
        Task<ILoanInformation> UpdateEmail(string type, string loanNumber, string id, IEmail email);
        Task<ILoanInformation> UpdatePhone(string type, string loanNumber, string id, IPhone phone);
        Task<ILoanInformation> UpdateApplicant(string loanNumber, string applicantId, IApplicantRequest applicantRequest);
        Task<ILoanInformation> UpdateBusiness(string loanNumber, string businessId, IBusinessDetails businessDetails);
        Task<IBankDetails> AddBankDetails(string loanNumber, IBankDetails bankDetails);
        Task<IBankDetails> UpdateBankDetails(string loanNumber, string bankId, IBankDetails bankDetails);
        Task<IBankDetails> GetBankDetails(string loanNumber, string bankId = null);
        Task<List<IBankDetails>> GetAllBankDetails(string loanNumber);
        Task ChangeStatus(string loanNumber, string status, List<string> reasons = null);
        Task ChangeStatusToClose(string loanNumber, List<string> reasons= null);
        Task<ILoanInformation> UpdatePaymentInstrument(string loanNumber, string paymentInstrumentId, IPaymentInstrument paymentInstrument);

        Task<ILoanInformation> UpdateAutoPayDetail(string loanNumber, IAutoPayRequest autoPayRequest);
        Task<ILoanInformation> AddOtherContact(string loanNumber, IOtherContact otherContact);
        Task<ILoanInformation> UpdateOtherContact(string loanNumber, string otherContactId, IOtherContact otherContact);
        Task<ILoanInformation> AddApplicantDetails(string loanNumber, List<ApplicantRequest> ApplicantRequest);
        Task<ILoanInformation> UpdateCollectionStage(string loanNumber, string collectionStage);
        Task<IBankDetails> SetBankActive(string loanNumber, string bankId);
        Task<List<ILoanDetails>> GetLoanList(DateTime startDate, DateTime endDate);
        Task<List<RelatedLoanResponse>> GetRelatedLoanInformation(string loanNumber);
        Task<ILoanInformation> UpdateRenewalDetails(string loanNumber, UpdateRenewalDetailRequest renewalDetail);
        Task<ILoanInformation> GetLoanInformationByLoanReferenceNumber(string loanReferenceNumber);
        Task<ILoanInformation> UpdateInvoiceDetails(string loanNumber, string invoiceId,List<ILoanInvoiceDetails> invoiceDetails);
        Task Unwind(string loanNumber, IUnwindRequest request);
        Task<string> GetMADFlagStatus(string applicationNumber);
        Task SendBorrowerInvite(IBorrowerEmailData borrowerData);
    }
}
