﻿namespace LMS.LoanManagement.Abstractions
{
    public class LoanModCreated
    {
        public string EntityType { get; set; }
        public string LoanNumber { get; set; }
        public string ProductId { get; set; }
        public object LoanResponse { get; set; }
        public object ScheduleResponse { get; set; }
        public object AccountingResponse { get; set; }
    }
}
