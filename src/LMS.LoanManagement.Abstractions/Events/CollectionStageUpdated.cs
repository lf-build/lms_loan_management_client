﻿namespace LMS.LoanManagement.Abstractions
{
    public class CollectionStageUpdated
    {
        public string EntityType { get; set; }
        public string LoanNumber { get; set; }
        public string ProductId { get; set; }
        public object LoanResponse { get; set; }
        public string OldCollectionStage { get; set; }
    }
}
