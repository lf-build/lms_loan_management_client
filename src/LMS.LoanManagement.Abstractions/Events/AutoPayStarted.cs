﻿namespace LMS.LoanManagement.Abstractions
{
    public class AutoPayStarted
    {
        public string EntityType { get; set; }
        public string LoanNumber { get; set; }
        public string ProductId { get; set; }

        public ILoanInformation LoanInformation { get; set; }
    }
}
