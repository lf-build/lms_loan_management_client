﻿namespace LMS.LoanManagement.Abstractions
{
    public class LoanOnboarded
    {
        public string EntityType { get; set; }
        public string LoanNumber { get; set; }
        public string ProductId { get; set; }
    }
}
