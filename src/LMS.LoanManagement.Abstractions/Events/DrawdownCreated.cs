﻿namespace LMS.LoanManagement.Abstractions
{
    public class DrawdownCreated
    {
        public string EntityType { get; set; }
        public string LoanNumber { get; set; }
        public string ProductId { get; set; }
        public object Response { get; set; }
    }
}
