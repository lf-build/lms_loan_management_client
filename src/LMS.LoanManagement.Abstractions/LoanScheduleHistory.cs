﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public class LoanScheduleHistory : LoanSchedule, ILoanScheduleHistory
    {
        public LoanScheduleHistory(ILoanSchedule loanSchedule)
        {
            LoanScheduleNumber = loanSchedule.LoanScheduleNumber;
            LoanNumber = loanSchedule.LoanNumber;
            ScheduleVersionNo = loanSchedule.ScheduleVersionNo;
            ScheduledCreatedReason = loanSchedule.ScheduledCreatedReason;
            ScheduleNotes = loanSchedule.ScheduleNotes;
            ScheduleVersionDate = loanSchedule.ScheduleVersionDate;
            LoanAmount = loanSchedule.LoanAmount;
            FundedAmount = loanSchedule.FundedAmount;
            FundedDate = loanSchedule.FundedDate;
            FirstPaymentDate = loanSchedule.FirstPaymentDate;
            PaymentFrequency = loanSchedule.PaymentFrequency;
            DailyRate = loanSchedule.DailyRate;
            AnnualRate = loanSchedule.AnnualRate;
            Tenure = loanSchedule.Tenure;
            FactorRate = loanSchedule.FactorRate;
            PaymentFrequencyRate = loanSchedule.PaymentFrequencyRate;
            CreditLimit = loanSchedule.CreditLimit;
            IsCurrent = loanSchedule.IsCurrent;
            TotalDrawDown = loanSchedule.TotalDrawDown;
            if(loanSchedule.ScheduleDetails != null)
            {
                ScheduleDetails = new List<ILoanScheduleDetails>();
                ScheduleDetails.AddRange(loanSchedule.ScheduleDetails);
            }
            if(loanSchedule.Fees != null)
            {
                Fees = new List<IFee>();
                Fees.AddRange(loanSchedule.Fees);
            }
            if (loanSchedule.FeeDetails != null)
            {
                FeeDetails = new List<IFeeDetails>();
                FeeDetails.AddRange(loanSchedule.FeeDetails);
            }
        }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDateTime { get; set; }
    }
}
