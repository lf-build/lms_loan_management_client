﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public interface IPayOffFeeDiscount : IAggregate
    {
        string LoanNumber { get; set; }       
        string FeeDiscountId { get; set; }

        string FeeType { get; set; }
        List<IPayOffFeeScheduleDetails> PayOffFeeScheduleDetails { get; set; }
     
      
    }
}