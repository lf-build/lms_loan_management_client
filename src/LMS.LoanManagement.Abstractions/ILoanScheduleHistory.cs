﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanScheduleHistory : IAggregate, ILoanSchedule
    {
        string UpdatedBy { get; set; }
        DateTime UpdatedDateTime { get; set; }
    }
}