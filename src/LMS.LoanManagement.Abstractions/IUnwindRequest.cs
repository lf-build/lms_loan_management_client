namespace LMS.LoanManagement.Abstractions
{
    public interface IUnwindRequest
    {
        bool UnwindFeesAtOrigination { get; set; }
    }
}