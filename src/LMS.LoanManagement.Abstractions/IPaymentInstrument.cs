﻿namespace LMS.LoanManagement.Abstractions
{
    public interface IPaymentInstrument
    {
        string Id { get; set; }
        bool IsActive { get; set; }
        PaymentInstrumentType PaymentInstrumentType { get; set; }
        string MandateNumber{get;set;} 
        string MandateType{get;set;} 
    }
}