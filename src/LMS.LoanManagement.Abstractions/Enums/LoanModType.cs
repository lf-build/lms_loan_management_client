﻿namespace LMS.LoanManagement.Abstractions
{
    public enum LoanModType
    {
        FixedPrincipal,
        FixedPayment
    }
}
