﻿namespace LMS.LoanManagement.Abstractions
{
    public enum EmailType
    {
        Personal,
        Work,
        Home,
        Others
    }
}
