﻿namespace LMS.LoanManagement.Abstractions
{
    public enum Currency
    {
        Dollar,
        Rupee,
        Yen,
        Dinar,
        Euro,
        Pound
    }
}
