﻿namespace LMS.LoanManagement.Abstractions
{
    public enum PhoneType
    {
        Residence,
        Mobile,
        Work,
        Alternate,
        Home,
        Others
    }
}
