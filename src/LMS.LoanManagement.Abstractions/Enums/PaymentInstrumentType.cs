﻿namespace LMS.LoanManagement.Abstractions
{
    public enum PaymentInstrumentType
    {
        Ach,
        Check,
        Cash,
        Wire,
        MoneyOrder,
        RTGS,
        NEFT,
        UPI,
        EkoPay1MultiLink,
        TDS

    }
}
