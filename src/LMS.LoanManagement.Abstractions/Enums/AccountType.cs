﻿namespace LMS.LoanManagement.Abstractions
{
    public enum AccountType
    {
        Savings,
        Current,
        Checking
    }
}
