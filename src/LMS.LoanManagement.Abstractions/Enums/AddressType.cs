﻿namespace LMS.LoanManagement.Abstractions
{
    public enum AddressType
    {
        Home,
        Mailing,
        Business,
        Military,
        Others
    }
}
