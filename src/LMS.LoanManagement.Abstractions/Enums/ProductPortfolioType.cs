﻿namespace LMS.LoanManagement.Abstractions
{
    public enum ProductPortfolioType
    {
        Installment,
        Mortgage,
        LineOfCredit,
        Open,
        Revolving,
        MCA,
        MCALOC,
        SCF
    }
}
