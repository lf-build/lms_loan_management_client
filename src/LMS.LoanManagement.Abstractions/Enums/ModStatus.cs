﻿namespace LMS.LoanManagement.Abstractions
{
    public enum ModStatus
    {
        None,
        Created,
        Active,
        Deleted
    }
}
