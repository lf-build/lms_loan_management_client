﻿using LendFoundry.Security.Tokens;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanOnboardingRepositoryFactory
    {
        ILoanOnboardingRepository Create(ITokenReader reader);
    }
}