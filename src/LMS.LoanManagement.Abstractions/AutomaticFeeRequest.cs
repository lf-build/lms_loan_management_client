﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public class AutomaticFeeRequest : IAutomaticFeeRequest
    {

     public   double PartialFeeAmount { get; set; }
     public   double OutStandingFeeAmount { get; set; }
    }
}
