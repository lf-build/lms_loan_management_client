﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace LMS.LoanManagement.Abstractions {
    public class BusinessRequest : IBusinessRequest {
        public string BusinessName { get; set; }
        public string DBA { get; set; }
        public string FedTaxId { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IAddress, Address>))]
        public IAddress Address { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IPhone, Phone>))]
        public IPhone Phone { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IEmail, Email>))]
        public IEmail Email { get; set; }
        public bool IsPrimary { get; set; }
        public DateTimeOffset EstablishedDate { get; set; }
        public double BusinessMaxLimitAmount{get;set;}

        [JsonConverter (typeof (InterfaceListConverter<IIdentifier, Identifier>))]
        public List<IIdentifier> BusinessIdentifiers { get; set; }
    }
}