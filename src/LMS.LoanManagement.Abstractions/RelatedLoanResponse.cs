using LendFoundry.Foundation.Date;

namespace LMS.LoanManagement.Abstractions
{
    public class RelatedLoanResponse
    {
        public string LoanApplicationNumber { get; set; }
        public TimeBucket RenewedOn { get; set; }
        public string LoanNumber { get; set; }
        public double RenewedAmount { get; set; }
        public string Status { get; set; }
        public string ReasonOfStatus { get; set; }
          public int SequenceNumber { get; set; }
        public string LoanStatusWorkFlowId { get; set; }
        public string ApplicationStatusWorkFlowId { get; set; }
        public string ProductId { get; set; }
    }
}