﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public class Settings
    {
        //public static string ServiceName { get; } = "loan-management";
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "loan-management";
      
    }
}
