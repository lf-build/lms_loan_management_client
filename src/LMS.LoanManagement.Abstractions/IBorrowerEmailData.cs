namespace LMS.LoanManagement.Abstractions
{
    public interface IBorrowerEmailData
    {
        string EntityType {get;set;}
        string BorrowerId {get;set;}
        string PortalUrl {get;set;}
        string BorrowerEmail {get;set;}
        string TemplateName {get;set;}
        string TemplateVersion {get;set;}
        string BorrowerName {get;set;}
    }
}