﻿using LendFoundry.StatusManagement;
using LMS.Foundation.Amortization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;


namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanScheduleService
    {
        Task<ILoanSchedule> LoanSchedule(ILoanScheduleRequest loanScheduleRequest);
        Task<ILoanSchedule> UpdateLoanSchedule(string loanNumber, string scheduleId, ILoanScheduleRequest loanScheduleRequest);
        Task InitializeLoan(string loanNumber,bool AutoAccrualPastDate);
        Task<Tuple<List<ILoanScheduleDetails>, List<ILoanScheduleDetail>>> Amortize(ILoanScheduleRequest loanScheduleRequest, double loanAmount, double factorRate = 0.0, double paymentAmount = 0.0, double remainingInterest = 0,bool isLoanMod = false, DateTime? calculatedFundedDate = null);
        List<ILoanScheduleDetails> Amortize(ILoanScheduleRequest loanScheduleRequest, double loanAmount, string portfolio, string roundingMethod, int roundingDigit, double factorRate = 0.0, double paymentAmount = 0.0);
        Task<Stream> DownloadAmortizationSchedule(ILoanScheduleRequest loanScheduleRequest);
        Task<ILoanSchedule> GetLoanSchedule(string loanNumber, string version = null);
        Task<List<ILoanScheduleDetails>> GetScheduleDetail(string loanNumber, string version, IProcessingDateRequst prosessingDate);
        Task<IStatusResponse> GetLoanStatus(string loanNumber, ILoanInformation loanInformation = null);
        Task<List<ILoanSchedule>> GetLoanScheduleByLoanNumber(string loanNumber);
        Task<ILoanInformation> AddDrawDownDetails(string loanNumber, IDrawDownDetails drawDownDetails);
        Task EnsureInitializeValid(string loanNumber, ILoanInformation loanInformation = null, ILoanSchedule loanSchedule = null);
        Task<ProductDetails> GetProductParameters(string productId, string loanNumber);
        Task<IFeeDetails> AddFeeDetails(string loanNumber, IFeeDetails feeDetails);
        Task<ILoanSchedule> UpdateFeeDetails(string loanNumber, string id, IFeeDetails feeDetails);
        Task<ILoanSchedule> UpdatePaymentDetails(string loanNumber, string id, IPaymentScheduleRequest paymentScheduleRequest);
        Task<ILoanSchedule> UpdatePaidDetails(string loanNumber, ILoanSchedule loanSchedule);
        Task<bool> AutomaticFeeSchedule(string loanNumber, IAutomaticFeeRequest automaticFeeRequest);
        Task<IFeeDetails> DeleteFeeDetails(string loanNumber, string feeId);
        Task<ILoanSchedule> ScheduleMissed(string loanNumber, string id, IPaymentScheduleRequest paymentScheduleRequest);
        Task<List<IPayOffFeeDiscount>> GetPayOffFeeDiscountSchedule(string loanNumber, string feeType = null);
        double GetPaymentAmount(ILoanScheduleRequest schedule);
        Task SetLoanModActive(string loanNumber, string loanScheduleId);
        Task SendPayOffSchedule(string loanNumber, string version, List<PayOffResponse> payOffList);
        Task<ILoanSchedule> SelectLoanModification(string loanNumber, ILoanModificationActivationRequest loanModificationActivationRequest);
        Task<List<ILoanScheduleDetails>> GetLoanModification(string loanNumber, ILoanModificationRequest loanModificationRequest);
        Task<List<ILoanScheduleDetails>> AdHocReamortization(string loanNumber, ILoanModificationRequest loanModificationRequest);
        Task DeleteLoanMod(string loanNumber, string loanScheduleId);
        Task<ILoanSchedule> UpdateLoanModification(string loanNumber, string loanScheduleId, ILoanModificationActivationRequest loanModificationActivationRequest);
        Task CloseOldLoan(LoanInformationRequest loanInformationRequest, ILoanInformation previousLoanDetails);
        Task<IPayOffFeeDiscount> AddPayOffDiscount (IPayOffFeeDiscount payoffDiscount);
        Task<List<ILoanSchedule>> GetLoanSchedules(List<string> loanNumbers);
    }
}
