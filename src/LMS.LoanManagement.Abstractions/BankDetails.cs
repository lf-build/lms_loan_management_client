﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;

namespace LMS.LoanManagement.Abstractions {
    public class BankDetails : Aggregate, IBankDetails {
        public string PaymentInstrumentId { get; set; }
        public string Name { get; set; }
        public string AccountNumber { get; set; }
        public string RoutingNumber { get; set; }
        public string BankHolderName { get; set; }
        public AccountType AccountType { get; set; }
        public string LoanNumber { get; set; }
        public bool IsActive { get; set; }
        public bool IsVerified { get; set; }
        public string BankId { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IIdentifier, Identifier>))]
        public List<IIdentifier> BranchIdentifiers { get; set; }

    }
}