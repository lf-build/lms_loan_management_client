﻿using System;

namespace LMS.LoanManagement.Abstractions
{
    public interface IIdentifier
    {
         string Id { get; set; }
         string Type { get; set; }
    }
}
