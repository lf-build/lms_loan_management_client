﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LMS.LoanManagement.Abstractions
{
    public interface IPayOffFeeScheduleDetails
    {

        DateTime PayOffFromDate { get; set; }

        DateTime PayOffToDate { get; set; }
        double  Amount { get; set; }
      
    }
}