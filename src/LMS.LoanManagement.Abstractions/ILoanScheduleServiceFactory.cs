﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LMS.Foundation.Amortization;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanScheduleServiceFactory
    {
        ILoanScheduleService Create(ITokenReader reader, ITokenHandler handler, ILogger logger);
    }
}
