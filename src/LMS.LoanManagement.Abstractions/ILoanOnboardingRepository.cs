﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Abstractions
{
    public interface ILoanOnboardingRepository : IRepository<ILoanInformation>
    {
        Task<ILoanInformation> GetLoanInformationByApplicationNumber(string applicationNumber);
        Task<ILoanInformation> GetLoanInformationByLoanNumber(string loanNumber);
        Task<ILoanInformation> UpdateLoanInformation(string loanNumber, ILoanInformation loanInformation);
        Task<ILoanInformation> UpdateAddress(string type, string loanNumber, string applicantId, IAddress address);
        Task<ILoanInformation> UpdatePhone(string type, string loanNumber, string applicantId, IPhone phone);
        Task<ILoanInformation> UpdateEmail(string type, string loanNumber, string applicantId, IEmail email);
        Task<ILoanInformation> UpdateApplicant(string loanNumber, string applicantId, IApplicantDetails applicantDetails);
        Task<ILoanInformation> UpdateBusiness(string loanNumber, string businessId, IBusinessDetails businessDetails);
        Task<ILoanInformation> UpdatePaymentInstrument(string loanNumber, string paymentInstrumentId, IPaymentInstrument paymentInstrument);
        Task<ILoanInformation> UpdateOtherContact(string loanNumber, string otherContactId, IOtherContact otherContact);
        Task<ILoanInformation> AddApplicant(string loanApplicationNumber, List<IApplicantDetails> applicantDetails);
        Task<List<ILoanInformation>> GetLoanInformationListByOnboardingDate(DateTime startDate, DateTime endDate);
        Task<List<ILoanInformation>> GetRelatedLoanInformation(string loanNumber);
        Task<ILoanInformation> UpdateRenewalDetails(string loanNumber, UpdateRenewalDetailRequest loanInformation);

        Task<ILoanInformation> GetLoanInformationByLoanReferenceNumber(string loanReferenceNumber);
        Task<ILoanInformation> UpdateInvoiceDetails(string loanNumber, string invoiceId,List<ILoanInvoiceDetails> invoiceDetails);

    }
}