﻿using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace LMS.LoanManagement.Client
{
    public class LoanManagementClientServiceFactory : ILoanManagementClientServiceFactory
    {
        public LoanManagementClientServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public LoanManagementClientServiceFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; }
        private Uri Uri { get; }
        public ILoanManagementClientService Create(ITokenReader reader)
        {
            var loanOnboardingClientServiceFactory = Provider.GetService<ILoanOnboardingClientServiceFactory>();
            var loanScheduleClientServiceFactory = Provider.GetService<ILoanScheduleClientServiceFactory>();
            return new LoanManagementClientService(loanOnboardingClientServiceFactory.Create(reader), loanScheduleClientServiceFactory.Create(reader));
        }

    }
}
