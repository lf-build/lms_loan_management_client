﻿using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LMS.LoanManagement.Abstractions;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LMS.LoanManagement.Client
{
    public class LoanScheduleClientServiceFactory : ILoanScheduleClientServiceFactory
    {
        public LoanScheduleClientServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }
        public LoanScheduleClientServiceFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; }
        private Uri Uri { get; }  

        public ILoanScheduleService Create(ITokenReader reader)
        {

            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("loan_management");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new LoanScheduleClientService(client);
        }
       
    }
}
