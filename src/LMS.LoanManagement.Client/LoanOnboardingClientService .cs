﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using RestSharp;
using System.Linq;
using LMS.LoanManagement.Abstractions;
using LendFoundry.Foundation.Client;

namespace LMS.LoanManagement.Client
{
    public class LoanOnboardingClientService : ILoanOnboardingService
    {
        public LoanOnboardingClientService(IServiceClient client) { Client = client; }

        private IServiceClient Client { get; }

        #region Status

        public async Task ChangeStatus(string loanNumber, string status, List<string> reasons = null)
        {
            var request = new RestRequest("/status/{loanNumber}/{status}", Method.POST);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(status), status);
            AppendReasons(request, reasons);
            await Client.ExecuteAsync(request);
        }

        public async Task ChangeStatusToClose(string loanNumber, List<string> reasons = null)
        {
            var request = new RestRequest("/status/close/{loanNumber}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            AppendReasons(request, reasons);
            await Client.ExecuteAsync(request);
        }

        #endregion

        #region Bank

        public async Task<List<IBankDetails>> GetAllBankDetails(string loanNumber)
        {
            var request = new RestRequest("/all/bank/{loanNumber}", Method.GET);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            var bankDetails = await Client.ExecuteAsync<List<BankDetails>>(request);
            return bankDetails.ToList<IBankDetails>();
        }

        public async Task<IBankDetails> GetBankDetails(string loanNumber, string bankId = null)
        {
            var request = new RestRequest("/bank/{loanNumber}", Method.GET);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            if (!string.IsNullOrWhiteSpace(bankId))
            {
                request = new RestRequest("/bank/{loanNumber}/{bankId}", Method.GET);
                request.AddUrlSegment(nameof(loanNumber), loanNumber);
                request.AddUrlSegment(nameof(bankId), bankId);
            }
            return await Client.ExecuteAsync<BankDetails>(request);
        }

        public async Task<IBankDetails> AddBankDetails(string loanNumber, IBankDetails bankDetails)
        {
            var request = new RestRequest("/bank/{loanNumber}", Method.POST);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddJsonBody(bankDetails);
            return await Client.ExecuteAsync<BankDetails>(request);
        }

        public async Task<IBankDetails> UpdateBankDetails(string loanNumber, string bankId, IBankDetails bankDetails)
        {
            var request = new RestRequest("/bank/{loanNumber}/{bankId}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(bankId), bankId);
            request.AddJsonBody(bankDetails);
            return await Client.ExecuteAsync<BankDetails>(request);
        }

        public async Task<IBankDetails> SetBankActive(string loanNumber, string bankId)
        {
            var request = new RestRequest("/bank/{loanNumber}/{bankId}/active", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(bankId), bankId);
            return await Client.ExecuteAsync<BankDetails>(request);
        }

        #endregion

        #region Loan

        public async Task<ILoanInformation> GetLoanInformationByApplicationNumber(string applicationNumber)
        {
            var request = new RestRequest("/application/{applicationNumber}", Method.GET);
            request.AddUrlSegment(nameof(applicationNumber), applicationNumber);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

        public async Task<ILoanInformation> GetLoanInformationByLoanNumber(string loanNumber)
        {
            var request = new RestRequest("/{loanNumber}", Method.GET);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

        public async Task<ILoanInformation> OnBoard(LoanInformationRequest loanInformationRequest)
        {
            var request = new RestRequest("/", Method.POST);
            request.AddJsonBody(loanInformationRequest);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

        public async Task<ILoanInformation> UpdateLoanInformation(string loanNumber, LoanInformationRequest loanInformationRequest)
        {
            var request = new RestRequest("/{loanNumber}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddJsonBody(loanInformationRequest);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

        #endregion

        #region Update Borrower

        public async Task<ILoanInformation> UpdateApplicant(string loanNumber, string applicantId, IApplicantRequest applicantRequest)
        {
            var request = new RestRequest("/applicant/{loanNumber}/{applicantId}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(applicantId), applicantId);
            request.AddJsonBody(applicantRequest);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

        public async Task<ILoanInformation> UpdateBusiness(string loanNumber, string businessId, IBusinessDetails businessDetails)
        {
            var request = new RestRequest("/business/{loanNumber}/{businessId}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(businessId), businessId);
            request.AddJsonBody(businessDetails);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

        public async Task<ILoanInformation> UpdatePhone(string type, string loanNumber, string id, IPhone phone)
        {
            var request = new RestRequest("/" + type + "/email/{loanNumber}/{" + type + "Id}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(type + "Id", id);
            request.AddJsonBody(phone);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

        public async Task<ILoanInformation> UpdateEmail(string type, string loanNumber, string id, IEmail email)
        {
            var request = new RestRequest("/" + type + "/email/{loanNumber}/{" + type + "Id}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(type + "Id", id);
            request.AddJsonBody(email);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

        public async Task<ILoanInformation> UpdateAddress(string type, string loanNumber, string id, IAddress address)
        {
            var request = new RestRequest("/" + type + "/email/{loanNumber}/{" + type + "Id}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(type + "Id", id);
            request.AddJsonBody(address);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

        #endregion

        public async Task<ILoanInformation> AddOtherContact(string loanNumber, IOtherContact otherContact)
        {
            var request = new RestRequest("/{loanNumber}/othercontact", Method.POST);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddJsonBody(otherContact);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

        public async Task<ILoanInformation> UpdateOtherContact(string loanNumber, string otherContactId, IOtherContact otherContact)
        {
            var request = new RestRequest("/{loanNumber}/othercontact/{otherContactId}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(otherContactId), otherContactId);
            request.AddJsonBody(otherContact);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

        public async Task<ILoanInformation> UpdatePaymentInstrument(string loanNumber, string paymentInstrumentId, IPaymentInstrument paymentInstrument)
        {
            var request = new RestRequest("/paymentinstrument/{loanNumber}/{paymentInstrumentId}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(paymentInstrumentId), paymentInstrumentId);
            request.AddJsonBody(paymentInstrument);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

        public async Task<ILoanInformation> UpdateAutoPayDetail(string loanNumber, IAutoPayRequest autoPayRequest)
        {
            var request = new RestRequest("/{loanNumber}/autopay", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddJsonBody(autoPayRequest);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

        private static void AppendReasons(IRestRequest request, IReadOnlyList<string> reasons)
        {
            if (reasons == null || !reasons.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < reasons.Count; index++)
            {
                var tag = reasons[index];
                url = url + $"/{{reason{index}}}";
                request.AddUrlSegment($"reason{index}", tag);
            }
            request.Resource = url;
        }

        public Task<ILoanInformation> AddApplicantDetails(string loanNumber, List<ApplicantRequest> ApplicantRequest)
        {
            throw new NotImplementedException();
        }

        public async Task<ILoanInformation> UpdateCollectionStage(string loanNumber, string collectionStage)
        {
            var request = new RestRequest("/{loanNumber}/collection", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddJsonBody(collectionStage);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

        public async Task<List<RelatedLoanResponse>> GetRelatedLoanInformation(string loanNumber)
        {
             var request = new RestRequest("{loanNumber}/relatedloans", Method.GET);
             request.AddUrlSegment(nameof(loanNumber), loanNumber);

            return await Client.ExecuteAsync<List<RelatedLoanResponse>>(request);
        }

       public async Task<List<ILoanDetails>> GetLoanList(DateTime startDate, DateTime endDate)
        {
            var request = new RestRequest("/loans/{startdate}/{enddate}", Method.GET);
            request.AddUrlSegment(nameof(startDate), startDate.ToString());
            request.AddUrlSegment(nameof(endDate), endDate.ToString());

            return await Client.ExecuteAsync<List<ILoanDetails>>(request);
        }
         public async Task<ILoanInformation> UpdateRenewalDetails(string loanNumber, UpdateRenewalDetailRequest renewalDetail)
        {
            var request = new RestRequest("{loanNumber}/renewal", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddJsonBody(renewalDetail);

            return await Client.ExecuteAsync<LoanInformation>(request);
        }
         public async Task<ILoanInformation> GetLoanInformationByLoanReferenceNumber(string loanReferenceNumber)
        {
            var request = new RestRequest("/loan/{loanReferenceNumber}", Method.GET);
            request.AddUrlSegment(nameof(loanReferenceNumber), loanReferenceNumber);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }
         public async Task<ILoanInformation> UpdateInvoiceDetails(string loanNumber, string invoiceId,List<ILoanInvoiceDetails> invoiceDetails)
        {
            var request = new RestRequest("/invoicedetails/{loanNumber}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            if(!string.IsNullOrEmpty(invoiceId))
            {
             request = new RestRequest("/invoicedetails/{loanNumber}/{invoiceId}", Method.PUT);
             request.AddUrlSegment(nameof(loanNumber), loanNumber);
             request.AddUrlSegment(nameof(invoiceId), invoiceId);
            }
            request.AddJsonBody(invoiceDetails);
            return await Client.ExecuteAsync<LoanInformation>(request);
        }

         public async Task Unwind(string loanNumber, IUnwindRequest unwindRequest)
         {
             var request = new RestRequest("/{loanNumber}/unwind", Method.POST);
             request.AddUrlSegment(nameof(loanNumber), loanNumber);
             request.AddJsonBody(unwindRequest);
             await Client.ExecuteAsync(request);
         }
         public async Task<string> GetMADFlagStatus(string applicationNumber)
         {
             var request = new RestRequest("/{applicationNumber}/minimumamountdue/flag", Method.GET);
             request.AddUrlSegment(nameof(applicationNumber), applicationNumber);
             return await Client.ExecuteAsync<string>(request);
         }
         public async Task SendBorrowerInvite(IBorrowerEmailData borrowerData)
         {
             var request = new RestRequest("send/borrower/invite", Method.POST);
             request.AddJsonBody(borrowerData);
             await Client.ExecuteAsync(request);
         }

         
    }
}
