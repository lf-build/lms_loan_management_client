﻿using LMS.LoanManagement.Abstractions;

namespace LMS.LoanManagement.Client
{
    public interface ILoanManagementClientService : ILoanOnboardingService, ILoanScheduleService
    {
    }
}
