﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using LendFoundry.StatusManagement;
using LMS.Foundation.Amortization;
using LMS.LoanManagement.Abstractions;

namespace LMS.LoanManagement.Client
{
    public class LoanManagementClientService : ILoanManagementClientService
    {
        public LoanManagementClientService(ILoanOnboardingService loanOnboardingService, ILoanScheduleService loanScheduleService)
        {
            LoanOnboardingService = loanOnboardingService;
            LoanScheduleService = loanScheduleService;
        }

        private ILoanOnboardingService LoanOnboardingService { get; }
        private ILoanScheduleService LoanScheduleService { get; }

        public async Task<IBankDetails> AddBankDetails(string loanNumber, IBankDetails bankDetails)
        {
            return await LoanOnboardingService.AddBankDetails(loanNumber, bankDetails);
        }

        public async Task<ILoanInformation> AddDrawDownDetails(string loanNumber, IDrawDownDetails drawDownDetails)
        {
            return await LoanScheduleService.AddDrawDownDetails(loanNumber, drawDownDetails);
        }

        public async Task<IFeeDetails> AddFeeDetails(string loanNumber, IFeeDetails feeDetails)
        {
            return await LoanScheduleService.AddFeeDetails(loanNumber, feeDetails);
        }

        public async Task<IFeeDetails> DeleteFeeDetails(string loanNumber, string feeId)
        {
            return await LoanScheduleService.DeleteFeeDetails(loanNumber, feeId);
        }

        public async Task<ILoanInformation> AddOtherContact(string loanNumber, IOtherContact otherContact)
        {
            return await LoanOnboardingService.AddOtherContact(loanNumber, otherContact);
        }

        public async Task<Tuple<List<ILoanScheduleDetails>, List<ILoanScheduleDetail>>> Amortize(ILoanScheduleRequest loanScheduleRequest, double loanAmount, double factorRate = 0.0, double paymentAmount = 0.0, double remainingInterest = 0, bool isLoanMod = false, DateTime? calculatedFundedDate = null)
        {
            return await LoanScheduleService.Amortize(loanScheduleRequest, loanScheduleRequest.LoanAmount, factorRate, paymentAmount, remainingInterest, isLoanMod, calculatedFundedDate);
        }

        public async Task ChangeStatus(string loanNumber, string status, List<string> reasons = null)
        {
            await LoanOnboardingService.ChangeStatus(loanNumber, status);
        }

        public async Task ChangeStatusToClose(string loanNumber, List<string> reasons = null)
        {
            await LoanOnboardingService.ChangeStatusToClose(loanNumber, reasons);
        }

        public async Task<Stream> DownloadAmortizationSchedule(ILoanScheduleRequest loanScheduleRequest)
        {
            return await LoanScheduleService.DownloadAmortizationSchedule(loanScheduleRequest);
        }

        public async Task EnsureInitializeValid(string loanNumber, ILoanInformation loanInformation = null, ILoanSchedule loanSchedule = null)
        {
            await LoanScheduleService.EnsureInitializeValid(loanNumber, loanInformation, loanSchedule);
        }

        public async Task<List<IBankDetails>> GetAllBankDetails(string loanNumber)
        {
            return await LoanOnboardingService.GetAllBankDetails(loanNumber);
        }

        public async Task<IBankDetails> GetBankDetails(string loanNumber, string bankId = null)
        {
            return await LoanOnboardingService.GetBankDetails(loanNumber, bankId);
        }

        public async Task<ILoanInformation> GetLoanInformationByApplicationNumber(string applicationNumber)
        {
            return await LoanOnboardingService.GetLoanInformationByApplicationNumber(applicationNumber);
        }

        public async Task<ILoanInformation> GetLoanInformationByLoanNumber(string loanNumber)
        {
            return await LoanOnboardingService.GetLoanInformationByLoanNumber(loanNumber);
        }

        public async Task<ILoanSchedule> GetLoanSchedule(string loanNumber, string version = null)
        {
            return await LoanScheduleService.GetLoanSchedule(loanNumber, version);
        }

        public async Task<List<ILoanSchedule>> GetLoanScheduleByLoanNumber(string loanNumber)
        {
            return await LoanScheduleService.GetLoanScheduleByLoanNumber(loanNumber);
        }

        public async Task<IStatusResponse> GetLoanStatus(string loanNumber, ILoanInformation loanInformation = null)
        {
            return await LoanScheduleService.GetLoanStatus(loanNumber, loanInformation);
        }

        public Task<ProductDetails> GetProductParameters(string productId, string loanNumber)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ILoanScheduleDetails>> GetScheduleDetail(string loanNumber, string version, IProcessingDateRequst prosessingDate)
        {
            return await LoanScheduleService.GetScheduleDetail(loanNumber, version, prosessingDate);
        }

        public async Task InitializeLoan(string loanNumber,bool AutoAccrualPastDate)
        {
            await LoanScheduleService.InitializeLoan(loanNumber,AutoAccrualPastDate);
        }

        public async Task<ILoanSchedule> LoanSchedule(ILoanScheduleRequest loanScheduleRequest)
        {
            return await LoanScheduleService.LoanSchedule(loanScheduleRequest);
        }

        public async Task<List<IPayOffFeeDiscount>> GetPayOffFeeDiscountSchedule(string loanNumber, string feeType = null)
        {
            return await LoanScheduleService.GetPayOffFeeDiscountSchedule(loanNumber, feeType);
        }

        public async Task<ILoanInformation> OnBoard(LoanInformationRequest loanInformationRequest)
        {
            return await LoanOnboardingService.OnBoard(loanInformationRequest);
        }

        public async Task<ILoanInformation> UpdateAddress(string type, string loanNumber, string id, IAddress address)
        {
            return await LoanOnboardingService.UpdateAddress(type, loanNumber, id, address);
        }

        public async Task<ILoanInformation> UpdateApplicant(string loanNumber, string applicantId, IApplicantRequest applicantRequest)
        {
            return await LoanOnboardingService.UpdateApplicant(loanNumber, applicantId, applicantRequest);
        }

        public async Task<ILoanInformation> UpdateAutoPayDetail(string loanNumber, IAutoPayRequest autoPayRequest)
        {
            return await LoanOnboardingService.UpdateAutoPayDetail(loanNumber, autoPayRequest);
        }

        public async Task<IBankDetails> UpdateBankDetails(string loanNumber, string bankId, IBankDetails bankDetails)
        {
            return await LoanOnboardingService.UpdateBankDetails(loanNumber, bankId, bankDetails);
        }

        public async Task<ILoanInformation> UpdateBusiness(string loanNumber, string businessId, IBusinessDetails businessDetails)
        {
            return await LoanOnboardingService.UpdateBusiness(loanNumber, businessId, businessDetails);
        }

        public async Task<ILoanInformation> UpdateEmail(string type, string loanNumber, string id, IEmail email)
        {
            return await LoanOnboardingService.UpdateEmail(type, loanNumber, id, email);
        }

        public async Task<ILoanSchedule> UpdateFeeDetails(string loanNumber, string id, IFeeDetails feeDetails)
        {
            return await LoanScheduleService.UpdateFeeDetails(loanNumber, id, feeDetails);
        }

        public async Task<ILoanInformation> UpdateLoanInformation(string loanNumber, LoanInformationRequest loanInformationRequest)
        {
            return await LoanOnboardingService.UpdateLoanInformation(loanNumber, loanInformationRequest);
        }

        public async Task<ILoanSchedule> UpdateLoanSchedule(string loanNumber, string scheduleId, ILoanScheduleRequest loanScheduleRequest)
        {
            return await LoanScheduleService.UpdateLoanSchedule(loanNumber, scheduleId, loanScheduleRequest);
        }

        public async Task<ILoanInformation> UpdateOtherContact(string loanNumber, string otherContactId, IOtherContact otherContact)
        {
            return await LoanOnboardingService.UpdateOtherContact(loanNumber, otherContactId, otherContact);
        }

        public async Task<ILoanSchedule> UpdatePaidDetails(string loanNumber, ILoanSchedule loanSchedule)
        {
            return await LoanScheduleService.UpdatePaidDetails(loanNumber, loanSchedule);
        }

        public async Task<ILoanSchedule> UpdatePaymentDetails(string loanNumber, string id, IPaymentScheduleRequest paymentScheduleRequest)
        {
            return await LoanScheduleService.UpdatePaymentDetails(loanNumber, id, paymentScheduleRequest);
        }

        public async Task<ILoanInformation> UpdatePaymentInstrument(string loanNumber, string paymentInstrumentId, IPaymentInstrument paymentInstrument)
        {
            return await LoanOnboardingService.UpdatePaymentInstrument(loanNumber, paymentInstrumentId, paymentInstrument);
        }

        public async Task<ILoanInformation> UpdatePhone(string type, string loanNumber, string id, IPhone phone)
        {
            return await LoanOnboardingService.UpdatePhone(type, loanNumber, id, phone);
        }

        public async Task<ILoanInformation> AddApplicantDetails(string loanNumber, List<ApplicantRequest> ApplicantRequest)
        {
            return await LoanOnboardingService.AddApplicantDetails(loanNumber, ApplicantRequest);
        }
        public async Task<ILoanSchedule> SelectLoanModification(string loanNumber, ILoanModificationActivationRequest loanModificationActivationRequest)
        {
            return await LoanScheduleService.SelectLoanModification(loanNumber, loanModificationActivationRequest);
        }

        public async Task<List<ILoanScheduleDetails>> GetLoanModification(string loanNumber, ILoanModificationRequest loanModificationRequest)
        {
            return await LoanScheduleService.GetLoanModification(loanNumber, loanModificationRequest);
        }

        public async Task<List<ILoanScheduleDetails>> AdHocReamortization(string loanNumber, ILoanModificationRequest loanModificationRequest)
        {
            return await LoanScheduleService.AdHocReamortization(loanNumber, loanModificationRequest);
        }
        public async Task<ILoanSchedule> ScheduleMissed(string loanNumber, string id, IPaymentScheduleRequest paymentScheduleRequest)
        {
            return await LoanScheduleService.ScheduleMissed(loanNumber, id, paymentScheduleRequest);
        }

        public double GetPaymentAmount(ILoanScheduleRequest schedule)
        {
            throw new NotImplementedException();
        }

        public async Task SetLoanModActive(string loanNumber, string loanScheduleId)
        {
            await LoanScheduleService.SetLoanModActive(loanNumber, loanScheduleId);
        }

        public Task SendPayOffSchedule(string loanNumber, string version, List<PayOffResponse> payOffList)
        {
            throw new NotImplementedException();
        }

        public async Task<ILoanInformation> UpdateCollectionStage(string loanNumber, string collectionStage)
        {
            return await LoanOnboardingService.UpdateCollectionStage(loanNumber, collectionStage);
        }

        public List<ILoanScheduleDetails> Amortize(ILoanScheduleRequest loanScheduleRequest, double loanAmount, string portfolio, string roundingMethod, int roundingDigit, double factorRate = 0, double paymentAmount = 0)
        {
            return LoanScheduleService.Amortize(loanScheduleRequest, loanAmount, portfolio, roundingMethod, roundingDigit);
        }

        public async Task<IBankDetails> SetBankActive(string loanNumber, string bankId)
        {
            return await LoanOnboardingService.SetBankActive(loanNumber, bankId);
        }

        public async Task<List<ILoanDetails>> GetLoanList(DateTime startDate, DateTime endDate)
        {
            return await LoanOnboardingService.GetLoanList(startDate, endDate);

        }

        public async Task<List<RelatedLoanResponse>> GetRelatedLoanInformation(string loanNumber)
        {
            return await LoanOnboardingService.GetRelatedLoanInformation(loanNumber);
        }
        public async Task<ILoanInformation> UpdateRenewalDetails(string loanNumber, UpdateRenewalDetailRequest renewalDetail)
        {
            return await LoanOnboardingService.UpdateRenewalDetails(loanNumber, renewalDetail);
        }
        
        public async Task CloseOldLoan(LoanInformationRequest loanInformationRequest, ILoanInformation previousLoanDetails)
        {
            await LoanScheduleService.CloseOldLoan(loanInformationRequest, previousLoanDetails);
        }

        public async Task DeleteLoanMod(string loanNumber, string loanScheduleId)
        {
            await LoanScheduleService.DeleteLoanMod(loanNumber, loanScheduleId);
        }

        public async Task<ILoanSchedule> UpdateLoanModification(string loanNumber, string loanScheduleId, ILoanModificationActivationRequest loanModificationActivationRequest)
        {
            return await LoanScheduleService.UpdateLoanModification(loanNumber, loanScheduleId, loanModificationActivationRequest);
        }

        public async Task<bool> AutomaticFeeSchedule(string loanNumber, IAutomaticFeeRequest automaticFeeRequest)
        {
            return await LoanScheduleService.AutomaticFeeSchedule(loanNumber, automaticFeeRequest);
        }

        public async Task<IPayOffFeeDiscount> AddPayOffDiscount(IPayOffFeeDiscount payoffDiscount)
        {
            return await LoanScheduleService.AddPayOffDiscount(payoffDiscount);
        }
         public async Task<ILoanInformation> GetLoanInformationByLoanReferenceNumber(string loanReferenceNumber)
        {
            return await LoanOnboardingService.GetLoanInformationByLoanReferenceNumber(loanReferenceNumber);
        }
        public async Task<ILoanInformation> UpdateInvoiceDetails(string loanNumber, string invoiceId,List<ILoanInvoiceDetails> invoiceDetails)
        {
             return await LoanOnboardingService.UpdateInvoiceDetails(loanNumber,invoiceId,invoiceDetails);
        }

        public async Task Unwind(string loanNumber, IUnwindRequest request)
        {
            await LoanOnboardingService.Unwind(loanNumber,request);
        }

        public async Task<List<ILoanSchedule>> GetLoanSchedules(List<string> loanNumbers)
        {
            return await LoanScheduleService.GetLoanSchedules(loanNumbers);
        }
        public async Task<string> GetMADFlagStatus(string applicationNumber)
        {
            return await LoanOnboardingService.GetMADFlagStatus(applicationNumber);
        }
        public async Task SendBorrowerInvite(IBorrowerEmailData emailData)
        {
            await LoanOnboardingService.SendBorrowerInvite(emailData);
        }

    }
}
