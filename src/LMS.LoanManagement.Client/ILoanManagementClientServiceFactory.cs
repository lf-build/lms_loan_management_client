﻿using LendFoundry.Security.Tokens;

namespace LMS.LoanManagement.Client
{
    public interface ILoanManagementClientServiceFactory
    {
        ILoanManagementClientService Create(ITokenReader reader);
    }
}
