﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LMS.LoanManagement.Client
{
    public static class LoanOnboardingClientServiceExtensions
    {
        public static IServiceCollection AddLoanOnboardingService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<ILoanOnboardingClientServiceFactory>(p => new LoanOnboardingClientServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ILoanOnboardingClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddLoanOnboardingService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<ILoanOnboardingClientServiceFactory>(p => new LoanOnboardingClientServiceFactory(p, uri));
            services.AddSingleton(p => p.GetService<ILoanOnboardingClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddLoanOnboardingService(this IServiceCollection services)
        {
            services.AddSingleton<ILoanOnboardingClientServiceFactory>(p => new LoanOnboardingClientServiceFactory(p));
            services.AddSingleton(p => p.GetService<ILoanOnboardingClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
