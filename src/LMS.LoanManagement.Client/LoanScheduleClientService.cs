﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using RestSharp;
using System.IO;
using LendFoundry.StatusManagement;
using LMS.LoanManagement.Abstractions;
using System.Linq;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using LMS.Foundation.Amortization;

namespace LMS.LoanManagement.Client
{
    public class LoanScheduleClientService : ILoanScheduleService
    {
        public LoanScheduleClientService(IServiceClient client) { Client = client; }
        private IServiceClient Client { get; }

        public Task<ILoanInformation> AddDrawDownDetails(string loanNumber, IDrawDownDetails drawDownDetails)
        {
            throw new NotImplementedException();
        }

        public async Task<Tuple<List<ILoanScheduleDetails>, List<ILoanScheduleDetail>>> Amortize(ILoanScheduleRequest loanScheduleRequest, double loanAmount, double factorRate = 0.0, double paymentAmount = 0.0, double remainingInterest = 0, bool isLoanMod = false, DateTime? calculatedFundedDate = null)
        {
            var request = new RestRequest("/amortize", Method.POST);
            request.AddJsonBody(loanScheduleRequest);
            var loanSchedule = await Client.ExecuteAsync<Tuple<List<ILoanScheduleDetails>, List<ILoanScheduleDetail>>>(request);
            return loanSchedule;
        }

        public async Task<Stream> DownloadAmortizationSchedule(ILoanScheduleRequest loanScheduleRequest)
        {
            var request = new RestRequest("/download", Method.POST);
            request.AddJsonBody(loanScheduleRequest);
            return await Client.ExecuteAsync<Stream>(request);
        }

        public Task EnsureInitializeValid(string loanNumber, ILoanInformation loanInformation = null, ILoanSchedule loanSchedule = null)
        {
            throw new NotImplementedException();
        }

        public async Task<ILoanSchedule> GetLoanSchedule(string loanNumber, string version = null)
        {
            var request = new RestRequest("/{loanNumber}/schedule", Method.GET);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            if (!string.IsNullOrWhiteSpace(version))
            {
                request = new RestRequest("/{loanNumber}/schedule/{version}", Method.GET);
                request.AddUrlSegment(nameof(loanNumber), loanNumber);
                request.AddUrlSegment(nameof(version), version);

            }
            return await Client.ExecuteAsync<LoanSchedule>(request);
        }

        public async Task<List<ILoanSchedule>> GetLoanScheduleByLoanNumber(string loanNumber)
        {
            var request = new RestRequest("/schedule/{loanNumber}", Method.GET);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            var loanSchedule = await Client.ExecuteAsync<List<LoanSchedule>>(request);
            return loanSchedule.ToList<ILoanSchedule>();
        }

        public Task<IStatusResponse> GetLoanStatus(string loanNumber)
        {
            throw new NotImplementedException();
        }

        public Task<IStatusResponse> GetLoanStatus(string loanNumber, ILoanInformation loanInformation = null)
        {
            throw new NotImplementedException();
        }

        public Task<ProductDetails> GetProductParameters(string productId, string loanNumber)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ILoanScheduleDetails>> GetScheduleDetail(string loanNumber, string version, IProcessingDateRequst processingDate)
        {
            var request = new RestRequest("{loanNumber}/{version}/schedule", Method.POST);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(version), version);
            request.AddJsonBody(processingDate);

            var response = await Client.ExecuteAsync<List<LoanScheduleDetails>>(request);
            return new List<ILoanScheduleDetails>(response);
        }

        public async Task InitializeLoan(string loanNumber,bool AutoAccrualPastDate)
        {
            var request = new RestRequest("/initialize/{loanNumber}/{AutoAccrualPastDate}", Method.POST);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(AutoAccrualPastDate), AutoAccrualPastDate);
            await Client.ExecuteAsync(request);
        }

        public async Task<ILoanSchedule> LoanSchedule(ILoanScheduleRequest loanScheduleRequest)
        {
            var request = new RestRequest("/schedule", Method.POST);
            request.AddJsonBody(loanScheduleRequest);
            return await Client.ExecuteAsync<LoanSchedule>(request);
        }

        public async Task<ILoanSchedule> UpdateLoanSchedule(string loanNumber, string scheduleId, ILoanScheduleRequest loanScheduleRequest)
        {
            var request = new RestRequest("/schedule/{loanNumber}/{scheduleId}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(scheduleId), scheduleId);
            request.AddJsonBody(loanScheduleRequest);
            return await Client.ExecuteAsync<LoanSchedule>(request);
        }

        public async Task<IFeeDetails> AddFeeDetails(string loanNumber, IFeeDetails feeDetails)
        {
            var request = new RestRequest("/{loanNumber}/feedetails", Method.POST);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddJsonBody(feeDetails);
            return await Client.ExecuteAsync<FeeDetails>(request);
        }

        public async Task<ILoanSchedule> UpdateFeeDetails(string loanNumber, string id, IFeeDetails feeDetails)
        {
            var request = new RestRequest("/{loanNumber}/feedetails/{id}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(id), id);
            request.AddJsonBody(feeDetails);
            return await Client.ExecuteAsync<LoanSchedule>(request);
        }

        public async Task<ILoanSchedule> UpdatePaymentDetails(string loanNumber, string id, IPaymentScheduleRequest paymentScheduleRequest)
        {
            var request = new RestRequest("/{loanNumber}/scheduledetails/{id}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(id), id);
            request.AddJsonBody(paymentScheduleRequest);
            return await Client.ExecuteAsync<LoanSchedule>(request);
        }

        public async Task<ILoanSchedule> UpdatePaidDetails(string loanNumber, ILoanSchedule loanSchedule)
        {
            var request = new RestRequest("/{loanNumber}/paiddetails", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddJsonBody(loanSchedule);
            return await Client.ExecuteAsync<LoanSchedule>(request);
        }

        public async Task<IFeeDetails> DeleteFeeDetails(string loanNumber, string feeId)
        {
            var request = new RestRequest("/{loanNumber}/{feeId}/fee", Method.DELETE);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(feeId), feeId);
            return await Client.ExecuteAsync<FeeDetails>(request);
        }

        public async Task<ILoanSchedule> ScheduleMissed(string loanNumber, string id, IPaymentScheduleRequest paymentScheduleRequest)
        {
            var request = new RestRequest("/{loanNumber}/schedulemiss/{id}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(id), id);
            request.AddJsonBody(paymentScheduleRequest);
            return await Client.ExecuteAsync<LoanSchedule>(request);
        }

        public async Task<List<IPayOffFeeDiscount>> GetPayOffFeeDiscountSchedule(string loanNumber, string feeType = null)
        {
            var request = new RestRequest("/payoffschedule/{loanNumber}", Method.GET);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            if (!string.IsNullOrWhiteSpace(feeType))
            {
                request = new RestRequest("/payoffschedule/{loanNumber}/{feeType}", Method.GET);
                request.AddUrlSegment(nameof(loanNumber), loanNumber);
                request.AddUrlSegment(nameof(feeType), feeType);

            }
            var response = await Client.ExecuteAsync<List<PayOffFeeDiscount>>(request);
            return new List<IPayOffFeeDiscount>(response);
        }

        public async Task<ILoanSchedule> SelectLoanModification(string loanNumber, ILoanModificationActivationRequest loanModificationActivationRequest)
        {
            var request = new RestRequest("/{loanNumber}/loanmods/select", Method.POST);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddJsonBody(loanModificationActivationRequest);
            return await Client.ExecuteAsync<LoanSchedule>(request);
        }

        public async Task<List<ILoanScheduleDetails>> GetLoanModification(string loanNumber, ILoanModificationRequest loanModificationRequest)
        {
            var request = new RestRequest("/{loanNumber}/loanmods", Method.POST);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddJsonBody(loanModificationRequest);
            var loanSchedule = await Client.ExecuteAsync<List<LoanScheduleDetails>>(request);
            return loanSchedule.ToList<ILoanScheduleDetails>();
        }

        public async Task<List<ILoanScheduleDetails>> AdHocReamortization(string loanNumber, ILoanModificationRequest loanModificationRequest)
        {
            var request = new RestRequest("/{loanNumber}/adhoc", Method.POST);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddJsonBody(loanModificationRequest);
            var loanSchedule = await Client.ExecuteAsync<List<LoanScheduleDetails>>(request);
            return loanSchedule.ToList<ILoanScheduleDetails>();
        }

        public double GetPaymentAmount(ILoanScheduleRequest schedule)
        {
            throw new NotImplementedException();
        }

        public async Task SetLoanModActive(string loanNumber, string loanScheduleId)
        {
            var request = new RestRequest("/{loanNumber}/loanmods/{loanScheduleId}/activate", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(loanScheduleId), loanScheduleId);
            await Client.ExecuteAsync(request);
        }

        public Task SendPayOffSchedule(string loanNumber, string version, List<PayOffResponse> payOffList)
        {
            throw new NotImplementedException();
        }

        public List<ILoanScheduleDetails> Amortize(ILoanScheduleRequest loanScheduleRequest, double loanAmount, string portfolio, string roundingMethod, int roundingDigit, double factorRate = 0, double paymentAmount = 0)
        {
            var request = new RestRequest("/amortize/{portfolio}/{roundingMethod}/{roundingDigit}", Method.POST);
            request.AddUrlSegment(nameof(portfolio), portfolio);
            request.AddUrlSegment(nameof(roundingMethod), roundingMethod);
            request.AddUrlSegment(nameof(roundingDigit), roundingDigit.ToString());
            request.AddJsonBody(loanScheduleRequest);
            var loanSchedule = Client.Execute<List<LoanScheduleDetails>>(request);
            return loanSchedule.ToList<ILoanScheduleDetails>();
        }

        public async Task DeleteLoanMod(string loanNumber, string loanScheduleId)
        {
            var request = new RestRequest("/{loanNumber}/loanmods/{loanScheduleId}/reject", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(loanScheduleId), loanScheduleId);
            await Client.ExecuteAsync(request);
        }
       public async Task<ILoanSchedule> UpdateLoanModification(string loanNumber, string loanScheduleId, ILoanModificationActivationRequest loanModificationActivationRequest)
        {
            var request = new RestRequest("/{loanNumber}/loanmods/update/{loanScheduleId}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddUrlSegment(nameof(loanScheduleId), loanScheduleId);
            request.AddJsonBody(loanModificationActivationRequest);
            return await Client.ExecuteAsync<LoanSchedule>(request);
        }
        public Task CloseOldLoan(LoanInformationRequest loanInformationRequest, ILoanInformation previousLoanDetails)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> AutomaticFeeSchedule(string loanNumber, IAutomaticFeeRequest automaticFeeRequest)
        {
            var request = new RestRequest("/automaticfee/{loanNumber}", Method.PUT);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            request.AddJsonBody(automaticFeeRequest);
            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<IPayOffFeeDiscount> AddPayOffDiscount (IPayOffFeeDiscount payoffDiscount) {
            var request = new RestRequest ("/payoffschedule", Method.POST);
            //request.AddJsonBody (payoffDiscount);
            var json = JsonConvert.SerializeObject (payoffDiscount);
            request.AddParameter ("application/json", json, ParameterType.RequestBody);
            return await Client.ExecuteAsync<PayOffFeeDiscount> (request);
        }

        public async Task<List<ILoanSchedule>> GetLoanSchedules(List<string> loanNumbers)
        {
            var request = new RestRequest("/loan/schedules", Method.POST);
            request.AddJsonBody(loanNumbers);
            var result = await Client.ExecuteAsync<List<LoanSchedule>>(request);
            return result.ToList<ILoanSchedule>();
        }
    }
}
