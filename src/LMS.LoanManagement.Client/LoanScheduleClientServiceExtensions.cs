﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LMS.LoanManagement.Client
{
    public static class LoanScheduleClientServiceExtensions
    {
        public static IServiceCollection AddLoanScheduleService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<ILoanScheduleClientServiceFactory>(p => new LoanScheduleClientServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ILoanScheduleClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        public static IServiceCollection AddLoanScheduleService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<ILoanScheduleClientServiceFactory>(p => new LoanScheduleClientServiceFactory(p, uri));
            services.AddSingleton(p => p.GetService<ILoanScheduleClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddLoanScheduleService(this IServiceCollection services)
        {
            services.AddSingleton<ILoanScheduleClientServiceFactory>(p => new LoanScheduleClientServiceFactory(p));
            services.AddSingleton(p => p.GetService<ILoanScheduleClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
