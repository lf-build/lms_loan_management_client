﻿using LendFoundry.Security.Tokens;
using LMS.LoanManagement.Abstractions;

namespace LMS.LoanManagement.Client
{
    public interface ILoanScheduleClientServiceFactory
    {
        ILoanScheduleService Create(ITokenReader reader);
    }
}
