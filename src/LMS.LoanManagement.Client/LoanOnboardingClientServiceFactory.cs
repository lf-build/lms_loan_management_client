﻿using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LMS.LoanManagement.Abstractions;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LMS.LoanManagement.Client
{
    public class LoanOnboardingClientServiceFactory : ILoanOnboardingClientServiceFactory
    {
        public LoanOnboardingClientServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }
        public LoanOnboardingClientServiceFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; }
        private Uri Uri { get; }


        public ILoanOnboardingService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("loan_management");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new LoanOnboardingClientService(client);
        }
       
    }
}
