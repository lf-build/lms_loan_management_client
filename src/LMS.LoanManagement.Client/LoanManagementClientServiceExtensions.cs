﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LMS.LoanManagement.Client
{
    public static class LoanManagementClientServiceExtensions
    {
        public static IServiceCollection AddLoanManagementService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddLoanOnboardingService(endpoint, port);
            services.AddLoanScheduleService(endpoint, port);
            services.AddTransient<ILoanManagementClientServiceFactory>(p => new LoanManagementClientServiceFactory(p));
            services.AddTransient(p => p.GetService<ILoanManagementClientServiceFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }
        public static IServiceCollection AddLoanManagementService(this IServiceCollection services, Uri uri)
        {
            services.AddLoanOnboardingService(uri);
            services.AddLoanScheduleService(uri);
            services.AddTransient<ILoanManagementClientServiceFactory>(p => new LoanManagementClientServiceFactory(p));
            services.AddTransient(p => p.GetService<ILoanManagementClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddLoanManagementService(this IServiceCollection services)
        {
            services.AddLoanOnboardingService();
            services.AddLoanScheduleService();
            services.AddTransient<ILoanManagementClientServiceFactory>(p => new LoanManagementClientServiceFactory(p));
            services.AddTransient(p => p.GetService<ILoanManagementClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
